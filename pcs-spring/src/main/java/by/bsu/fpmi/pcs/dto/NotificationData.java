package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Event;

import java.text.SimpleDateFormat;

/**
 * Created by Alexandra on 22.12.2017.
 */
public class NotificationData {
    private int id;
    private String description;
    private String date;
    private String time;
    private String projectName;
    private int projectId;

    public NotificationData() {}
    public NotificationData(Event event) {
        this.id = event.getId();
        this.description = event.getDescription();
        this.projectId = event.getProject().getId();
        this.projectName = event.getProject().getName();
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
        if (event.getEventTime() != null) {
            try {
                this.date = sdf1.format(event.getEventTime().getTime());
                this.time = sdf2.format(event.getEventTime().getTime());
            } catch (Exception e) {
                this.date = null;
                this.time = null;
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}
