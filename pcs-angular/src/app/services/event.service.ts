/**
 * Created by Alexandra on 26.11.2017.
 */
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseData } from '../entities/response-data';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';
import { EventData } from "../entities/event-data";
import {ErrorService} from "./error.service";
import {NotificationData} from "../entities/notification-data";

@Injectable()
export class EventService {

  private baseUrl = `${environment.api.url}/api/projects`;
  private notificationsUrl = `${environment.api.url}/api/notifications`;

  eventsData: Subject<any> = new Subject();
  deleted: Subject<boolean> = new Subject();
  eventData: Subject<EventData> = new Subject();
  notificationsData: BehaviorSubject<NotificationData[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient, private authService: AuthService, private errorService: ErrorService){}

  getEvents(projectId: number) {
    this.http.get(`${this.baseUrl}/${projectId}/events`, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.eventsData.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  getNotifications() {
    this.http.get(this.notificationsUrl, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.notificationsData.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  saveEvent(event: EventData, projectId: number) {
    this.http.post(`${this.baseUrl}/${projectId}/events`, event, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.eventData.next(data.requestedData);
            this.getEvents(projectId);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  deleteEvent(eventId: number, projectId: number) {
    this.http.delete(`${this.baseUrl}/${projectId}/events/${eventId}`, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.deleted.next(true);
            this.getEvents(projectId);
          } else {
            this.deleted.next(false);
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.deleted.next(false);
          this.errorService.showHttpCodeError(err);
        });
  }

}

