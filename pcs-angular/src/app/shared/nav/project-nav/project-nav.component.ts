import{ Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'project-nav',
  templateUrl: './project-nav.component.html',
  styleUrls: ['./project-nav.component.scss']
})
export class ProjectNavComponent implements OnInit {
  @Input() projectId: number;
  @Input() match: boolean;

  constructor() {}

  ngOnInit(): void {
  }
}
