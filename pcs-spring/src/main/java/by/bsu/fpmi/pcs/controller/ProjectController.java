package by.bsu.fpmi.pcs.controller;

import by.bsu.fpmi.pcs.dto.*;
import by.bsu.fpmi.pcs.entity.User;
import by.bsu.fpmi.pcs.service.AuthService;
import by.bsu.fpmi.pcs.service.ProjectService;
import by.bsu.fpmi.pcs.service.ServiceException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Alexandra on 18.11.2017.
 */
@RestController
public class ProjectController {

    private static Logger logger = LogManager.getLogger(ProjectController.class);

    @Autowired
    AuthService authService;

    @Autowired
    ProjectService projectService;

    @PostConstruct
    public void init() {}

    @GetMapping("/api/projects")
    public ResponseData getProjects(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("GET /api/projects\tGetting projects with token: %s", token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("GET /api/projects\tUser with id %d requests getting projects", user.getId()));
            return new ResponseData(projectService.getUserProjects(user));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @GetMapping("/api/projects/{id}")
    public ResponseData getProject(@PathVariable int id, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("GET /api/projects/%d\tGetting a project with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("GET /api/projects/%d\tUser with id %d requests a project (id=%d)",
                    id, user.getId(), id));
            return new ResponseData(projectService.getProjectData(user, id));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/projects")
    public ResponseData saveProject(@RequestBody ProjectInfoData projectInfoData, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("POST /api/projects\tSaving project with token: %s", token));
        try {
            User user = authService.auth(token);
            if (projectInfoData.getId() > 0) {
                logger.info(String.format("POST /api/projects\tUser with id %d requests modifying project with id: %d",
                        user.getId(), projectInfoData.getId()));
                return new ResponseData(projectService.modifyProject(user, projectInfoData));
            }
            logger.info(String.format("POST /api/projects\tUser with id %d requests creating a new project (name=%s, description=%s)",
                    user.getId(), projectInfoData.getName(), projectInfoData.getDescription()));
            return new ResponseData(projectService.createNewProject(user, projectInfoData));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @DeleteMapping("/api/projects/{id}")
    public ResponseData deleteProject(@PathVariable int id, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("DELETE /api/projects/%d\tDeleting a project with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("DELETE /api/projects/%d\tUser with id %d requests deleting a project (id=%d)",
                    id, user.getId(), id));
            projectService.deleteProject(user, id);
            return new ResponseData();
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/projects/{id}/members")
    public ResponseData saveMembership(@PathVariable int id, @RequestBody MembershipData membershipData, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("POST /api/projects/%d/members\tSaving membership with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("POST /api/projects/%d/members\tUser with id %d requests saving a membership for a project (id=%d)",
                    id, user.getId(), id));
            return new ResponseData(projectService.saveMembership(user, id, membershipData));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @GetMapping("/api/projects/{id}/members")
    public ResponseData getMembers(@PathVariable int id, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("GET /api/projects/%d/members\tGet memberships with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("GET /api/projects/%d/members\tUser with id %d requests getting memberships for a project (id=%d)",
                    id, user.getId(), id));
            return new ResponseData(projectService.getMembers(user, id));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/projects/{id}/statuses")
    public ResponseData saveStatus(@PathVariable int id, @RequestBody StatusData statusData, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("POST /api/projects/%d/statuses\tSaving status with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("POST /api/projects/%d/statuses\tUser with id %d requests saving a status for a project (id=%d)",
                    id, user.getId(), id));
            return new ResponseData(projectService.addStatus(user, id, statusData));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }
}
