/**
 * Created by Alexandra on 26.11.2017.
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginData } from '../entities/login-data';
import { TokenData } from '../entities/token-data';
import { ResponseData } from '../entities/response-data';
import { environment } from '../../environments/environment';
import {ErrorService} from "./error.service";

@Injectable()
export class AuthService {

  private authUrl = `${environment.api.url}/api/auth`;
  private loginUrl = `${this.authUrl}/login`;
  private logoutUrl = `${this.authUrl}/logout`;
  private registerUrl = `${this.authUrl}/register`;

  tokenData: BehaviorSubject<TokenData> = new BehaviorSubject(null);

  constructor(private http: HttpClient, private errorService: ErrorService){}

  isAuthenticated(): boolean {
    return !!this.tokenData.getValue() && !!this.tokenData.getValue().accessToken;
  }

  getAccessToken(): string {
    return this.isAuthenticated() ? this.tokenData.getValue().accessToken : null;
  }

  setTokenData(newTokenData: TokenData): void {
    this.tokenData.next(newTokenData);
    sessionStorage.setItem(environment.auth.sessionKey, JSON.stringify(newTokenData));
  }

  login(loginData: LoginData) {
    this.http.post(this.loginUrl, loginData, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue)})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.setTokenData(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
      },
        err => {
          this.errorService.showHttpCodeError(err);
      });
  }

  register(loginData: LoginData) {
    this.http.post(this.registerUrl, loginData, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue)})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.setTokenData(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  logout() {
    this.http.post(this.logoutUrl, null, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.setTokenData(null);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }
}
