import { Component, OnInit, Input } from '@angular/core';
import { IssueData } from "../../../../entities/issue-data";
import { IssueType, types } from "../../../../entities/issue-types";
import { IssuePriority, priorities } from "../../../../entities/issue-priorities";
import { StatusData } from "../../../../entities/status-data";
import { ProjectService } from "../../../../services/project.service";
import { IssueService } from "../../../../services/issue.service";
import { MembershipData } from "../../../../entities/membership-data";


@Component({
  selector: 'create-issue-block',
  templateUrl: './create-issue-block.component.html',
  styleUrls: ['./create-issue-block.component.scss']
})
export class CreateIssueBlockComponent implements OnInit {
  issue: IssueData = new IssueData();
  types: IssueType[] = types;
  priorities: IssuePriority[] = priorities;
  @Input() cancel: Function;
  statuses: StatusData[] = [];
  projectId: number = 0;
  members: MembershipData[] = [];
  noAssigneeMembership: MembershipData = new MembershipData();

  constructor(private projectService: ProjectService, private issueService: IssueService) {
    this.noAssigneeMembership.userName = 'No assignee';
    this.noAssigneeMembership.userId = 0;
    this.members = [...projectService.members.getValue(), this.noAssigneeMembership];
    if (projectService.currentProject.getValue() && projectService.currentProject.getValue().statuses) {
      this.statuses = projectService.currentProject.getValue().statuses;
      this.issue.projectId = projectService.currentProject.getValue().shortProjectData.id;
      this.projectId = this.issue.projectId;
      if (this.statuses.length > 0 && !this.issue.statusId) {
        this.issue.statusId = this.statuses[0].id;
      }
    }
    this.projectService.currentProject.subscribe(pr => {
      this.statuses = pr && pr.statuses ? pr.statuses : [];
      this.issue.projectId = pr && pr.shortProjectData ? pr.shortProjectData.id : 0;
      this.projectId = this.issue.projectId;
      if (this.statuses.length > 0 && !this.issue.statusId) {
        this.issue.statusId = this.statuses[0].id;
      }
    });
    this.projectService.members.subscribe(ms => {
      this.members = [this.noAssigneeMembership];
      this.members.push(...ms);
    });
  }

  ngOnInit(): void {
  }

  onCancel() {
    this.issue = new IssueData();
    this.issue.projectId = this.projectId;
    this.cancel(false);
  }

  onTypeSelect(type) {
    this.issue.type = type;
  }
  onPrioritySelect(pr) {
    this.issue.priority = pr;
  }
  onStatusSelect(st) {
    this.issue.statusId = st;
  }
  onAssigneeSelect(user) {
    this.issue.userId = user;
  }

  onCreate() {
    this.issueService.saveIssue(this.issue);
    this.issue = new IssueData();
    this.issue.projectId = this.projectId;
  }
}
