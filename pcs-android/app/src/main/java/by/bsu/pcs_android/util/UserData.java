package by.bsu.pcs_android.util;


import org.json.JSONObject;

/**
 * Created by Alexandra on 18.11.2017.
 */
public class UserData {

    private int id;
    private String email;
    private String name;

    public UserData() {}

    public UserData(JSONObject user) {
        setId(user.optInt("id", 0));
        setEmail(user.optString("email", ""));
        setName(user.optString("name", ""));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
