/**
 * Created by Alexandra on 27.11.2017.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectInfoData } from "../../../entities/project-info-data";
import { StatusData } from "../../../entities/status-data";
import { ProjectService } from "../../../services/project.service";
import { ProjectGlobalData } from "../../../entities/project-global-data";

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: [ './settings.component.scss' ]
})
export class SettingsComponent implements OnInit {
  id: number;
  projectInfo: ProjectInfoData = new ProjectInfoData();
  statuses: StatusData[] = [];
  status: StatusData = new StatusData();
  isAdmin: boolean = true;

  constructor(private route: ActivatedRoute, private projectService: ProjectService, private router: Router) {
    if (projectService.currentProject.getValue()) {
      this.setProject(projectService.currentProject.getValue());
    }
    projectService.currentProject.subscribe(pr => this.setProject(pr));
  }

  setProject(project: ProjectGlobalData) {
    if (!project) {
      return;
    }
    if (!project.shortProjectData.isAdmin) {
      this.router.navigate(['/']);
    }
    this.isAdmin = project.shortProjectData.isAdmin;
    this.statuses = project.statuses;
    this.statuses.sort((s1, s2) => s1.order < s2.order ? -1 : 1);
    this.projectInfo.id = project.shortProjectData.id;
    this.projectInfo.name = project.shortProjectData.name;
    this.projectInfo.description = project.shortProjectData.description;
  }

  ngOnInit(): void {
    this.getSettings();
  }

  getSettings(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    // this.heroService.getHero(id)
    //   .subscribe(hero => this.hero = hero);
  }

  saveProjectInfo() {
    this.projectService.createProject(this.projectInfo);
  }

  createStatus() {
    this.status.order = this.statuses.length - 1;
    this.projectService.createStatus(this.projectInfo.id, this.status);
    this.status = new StatusData();
  }
}
