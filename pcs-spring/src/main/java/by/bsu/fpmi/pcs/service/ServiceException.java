package by.bsu.fpmi.pcs.service;

import by.bsu.fpmi.pcs.exception.PCSException;

/**
 * Created by Alexandra on 16.11.2017.
 */
public class ServiceException extends PCSException {
    public ServiceException(int code) {
        super(code);
    }

    public ServiceException(int code, String message) {
        super(code, message);
    }

    public ServiceException(int code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public ServiceException(int code, Throwable cause) {
        super(code, cause);
    }
}
