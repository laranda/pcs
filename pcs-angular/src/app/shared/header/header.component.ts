import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ProjectService } from '../../services/project.service';
import { UserData } from '../../entities/user-data';
import { ShortProjectData } from '../../entities/short-project-data';
import { ProjectGlobalData } from '../../entities/project-global-data';
import {EventService} from "../../services/event.service";


@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isAuthenticated: boolean = false;
  userData: UserData = null;
  projectsData: ShortProjectData[] = null;
  currentProjectId: number = null;
  hasNotifications: boolean = false;

  constructor(private authService: AuthService,
              private projectService: ProjectService,
              private eventService: EventService,
              private router: Router) {
    authService.tokenData.subscribe(val => {
      this.isAuthenticated = this.authService.isAuthenticated();
      this.userData = this.isAuthenticated ? val.userData : null;
    });
    this.hasNotifications = eventService.notificationsData.getValue().length > 0;
    eventService.notificationsData.subscribe(ns => this.hasNotifications = ns.length > 0);
    projectService.currentProject.subscribe((val: ProjectGlobalData) => {
      this.currentProjectId = val && val.shortProjectData ? val.shortProjectData.id : null;
    });
    projectService.projectsData.subscribe(val => {this.projectsData = val;});
  }

  ngOnInit(): void {
    this.projectsData = this.projectService.projectsData.getValue();
    this.currentProjectId = this.projectService.currentProject.getValue() ? this.projectService.currentProject.getValue().shortProjectData.id : null;
  }

  logout() {
    this.authService.logout();
  }

  onProjectSelect(projectId: number) {
    this.router.navigate([`project/${projectId}`]);
  }
}
