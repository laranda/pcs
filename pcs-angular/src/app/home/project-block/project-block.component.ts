import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ShortProjectData } from '../../entities/short-project-data'


@Component({
  selector: 'project-block',
  templateUrl: './project-block.component.html',
  styleUrls: ['./project-block.component.scss']
})
export class ProjectBlockComponent implements OnInit {
  @Input() project: ShortProjectData;

  constructor(private router: Router) {}

  ngOnInit(): void {
  }

  goToProjectPage() {
    if (this.project && this.project.id) {
      this.router.navigate([`project/${this.project.id}`]);
    }
  }
}
