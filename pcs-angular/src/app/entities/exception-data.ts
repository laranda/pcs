/**
 * Created by Alexandra on 26.11.2017.
 */

export class ExceptionData {
  code: number;
  message: string;
}
