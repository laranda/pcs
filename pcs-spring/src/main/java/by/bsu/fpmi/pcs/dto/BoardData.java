package by.bsu.fpmi.pcs.dto;

import java.util.List;

/**
 * Created by Alexandra on 28.11.2017.
 */
public class BoardData {
    private List<IssueData> goals;
    private List<StatusData> expedite;
    private List<StatusData> issues;

    public BoardData() {}

    public List<IssueData> getGoals() {
        return goals;
    }

    public void setGoals(List<IssueData> goals) {
        this.goals = goals;
    }

    public List<StatusData> getExpedite() {
        return expedite;
    }

    public void setExpedite(List<StatusData> expedite) {
        this.expedite = expedite;
    }

    public List<StatusData> getIssues() {
        return issues;
    }

    public void setIssues(List<StatusData> issues) {
        this.issues = issues;
    }
}
