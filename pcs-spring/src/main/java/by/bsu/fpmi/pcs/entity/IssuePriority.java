package by.bsu.fpmi.pcs.entity;

/**
 * Created by Alexandra on 28.11.2017.
 */
public class IssuePriority {
    public static final int TRIVIAL = 0;
    public static final int MINOR = 1;
    public static final int MAJOR = 2;
    public static final int CRITICAL = 3;
    public static final int BLOCKER = 4;
}
