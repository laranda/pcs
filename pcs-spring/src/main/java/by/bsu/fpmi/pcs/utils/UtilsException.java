package by.bsu.fpmi.pcs.utils;

import by.bsu.fpmi.pcs.exception.PCSException;

/**
 * Created by Alexandra on 16.11.2017.
 */
public class UtilsException extends PCSException {
    public UtilsException(int code) {
        super(code);
    }

    public UtilsException(int code, String message) {
        super(code, message);
    }

    public UtilsException(int code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public UtilsException(int code, Throwable cause) {
        super(code, cause);
    }
}
