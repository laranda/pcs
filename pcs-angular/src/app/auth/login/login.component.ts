import{ Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { LoginData } from '../../entities/login-data';

import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginData: LoginData = new LoginData();
  authSub: any = null;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/home']);
    }
    this.authSub = this.authService.tokenData.subscribe(value => {
      if (value && value.accessToken) {
        this.router.navigate(['/home']);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.authSub !== null) {
      this.authSub.unsubscribe();
    }
  }

  login() {
    this.authService.login(this.loginData);
  }
}
