package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.User;

/**
 * Created by Alexandra on 18.11.2017.
 */
public class UserData {

    private int id;
    private String email;
    private String name;

    public UserData() {}

    public UserData(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.name = user.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
