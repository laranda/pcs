/**
 * Created by Alexandra on 29.11.2017.
 */

export class IssueCriteria {
  statusId: number;
  projectId: number;
  description: string;
}
