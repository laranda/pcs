/**
 * Created by Alexandra on 27.11.2017.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MembershipData } from "../../../entities/membership-data";
import { ProjectService } from "../../../services/project.service";
import { RoleData } from "../../../entities/role-data";
import {RoleService} from "../../../services/role.service";

@Component({
  selector: 'team',
  templateUrl: './team.component.html',
  styleUrls: [ './team.component.scss' ]
})
export class TeamComponent implements OnInit {
  id: number;
  members: MembershipData[] = [];
  newMember: MembershipData = new MembershipData();
  roles: RoleData[] = [];
  isAdmin: boolean = true;

  constructor(private route: ActivatedRoute, private projectService: ProjectService, private roleService: RoleService) {
    projectService.members.subscribe(ms => this.members = ms);
    if (roleService.rolesData.getValue()) {
      this.roles = roleService.rolesData.getValue();
    }
    roleService.rolesData.subscribe(val => this.roles = val);
    this.projectService.currentProject.subscribe(val => {
      this.isAdmin = val ? val.shortProjectData.isAdmin : true;
    });
  }

  ngOnInit(): void {
    this.getTeam();
  }

  getTeam(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.projectService.getMembers(this.id);
    if (!!this.projectService.currentProject.getValue()){
      this.isAdmin = this.projectService.currentProject.getValue().shortProjectData.isAdmin;
    }
    // this.heroService.getHero(id)
    //   .subscribe(hero => this.hero = hero);
  }

  addMember(): void {
    this.projectService.addMember(this.id, this.newMember);
    this.newMember = new MembershipData();
  }

  onRoleSelect(roleId: number): void {
    this.newMember.roleId = roleId;
  }
}
