package by.bsu.pcs_android.dto;

public class NotificationData {
    private int id;
    private String description;
    private String date;
    private String time;
    private String projectName;
    private int projectId;

    public NotificationData() {}

    public NotificationData(int id, String description, String date, String time, String projectName, int projectId) {
        this.id = id;
        this.description = description;
        this.date = date;
        this.time = time;
        this.projectName = projectName;
        this.projectId = projectId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}
