package by.bsu.fpmi.pcs.repository;

import by.bsu.fpmi.pcs.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alexandra on 17.09.2017.
 */
public interface EventRepository extends JpaRepository<Event, Integer> {
    List<Event> findByProjectIdOrderByEventTimeAsc(int projectId);
}
