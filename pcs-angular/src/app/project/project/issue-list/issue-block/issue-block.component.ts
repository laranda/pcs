import { Component, OnInit, Input } from '@angular/core';
import { IssueData } from "../../../../entities/issue-data";
import { IssueType, types } from "../../../../entities/issue-types";
import { IssuePriority, priorities } from "../../../../entities/issue-priorities";


@Component({
  selector: 'issue-block',
  templateUrl: './issue-block.component.html',
  styleUrls: ['./issue-block.component.scss']
})
export class IssueBlockComponent implements OnInit {
  @Input() color: string = 'red';
  @Input() issue: IssueData = new IssueData();
  types: IssueType[] = types;
  priorities: IssuePriority[] = priorities;

  ngOnInit(): void {
  }
}
