package by.bsu.fpmi.pcs.controller;

import by.bsu.fpmi.pcs.dto.*;
import by.bsu.fpmi.pcs.entity.User;
import by.bsu.fpmi.pcs.service.AuthService;
import by.bsu.fpmi.pcs.service.IssueService;
import by.bsu.fpmi.pcs.service.ServiceException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Alexandra on 18.11.2017.
 */
@RestController
public class IssueController {

    private static Logger logger = LogManager.getLogger(IssueController.class);

    @Autowired
    AuthService authService;

    @Autowired
    IssueService issueService;

    @PostConstruct
    public void init() {}

    @GetMapping("/api/projects/{id}/board")
    public ResponseData getBoardIssues(@PathVariable int id, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("GET /api/projects/{id}/board\tGetting board issues with token: %s", token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("GET /api/projects/{id}/board\tUser with id %d requests getting board issues", user.getId()));
            return new ResponseData(issueService.getBoardIssues(user, id));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/issues/list")
    public ResponseData findIssuesByCriteria(@RequestBody IssueCriteria issueCriteria, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("POST /api/issues/list\tFind issues with token: %s", token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("POST /api/issues/list\tUser with id %d requests to find issues", user.getId()));
            return new ResponseData(issueService.getIssuesList(user, issueCriteria));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/issues")
    public ResponseData saveIssue(@RequestBody IssueData issueData, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("POST /api/issues\tSave issue with token: %s", token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("POST /api/issues\tUser with id %d requests to save issue", user.getId()));
            return new ResponseData(issueService.saveIssue(user, issueData));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @GetMapping("/api/issues/{id}")
    public ResponseData getIssue(@PathVariable int id, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("GET /api/issues/%d\tGet issue with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("GET /api/issues/%d\tUser with id %d requests to get issue", id, user.getId()));
            return new ResponseData(issueService.getIssue(user, id));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @DeleteMapping("/api/issues/{id}")
    public ResponseData deleteIssue(@PathVariable int id, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("DELETE /api/issues/%d\tDelete issue with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("DELETE /api/issues/%d\tUser with id %d requests to delete issue", id, user.getId()));
            issueService.deleteIssue(user, id);
            return new ResponseData();
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/issues/{id}/comments")
    public ResponseData addComment(@PathVariable int id, @RequestBody CommentData commentData, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("POST /api/issues/%d/comments\tAdd comment to issue with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("POST /api/issues/%d/comments\tUser with id %d requests to add new comment", id, user.getId()));
            return new ResponseData(issueService.addComment(user, id, commentData));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @GetMapping("/api/issues/{id}/comments")
    public ResponseData getComments(@PathVariable int id, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("GET /api/issues/%d/comments\tGet comments of issue with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("GET /api/issues/%d/comments\tUser with id %d requests to get comments", id, user.getId()));
            return new ResponseData(issueService.getComments(user, id));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }
}
