/**
 * Created by Alexandra on 26.11.2017.
 */

import {Directive, OnDestroy} from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from "@angular/router";
import { Location } from '@angular/common';

@Directive({
  selector: '[protected]'
})
export class ProtectedDirective implements OnDestroy {
  private sub: any = null;

  constructor(private authService:AuthService, private router:Router, private location:Location) {
    if (!authService.isAuthenticated()) {
      this.location.replaceState('/'); // clears browser history so they can't navigate with back button
      this.router.navigate(['/login']);
    }

    this.sub = this.authService.tokenData.subscribe((value) => {
      if (!value || !value.accessToken) {
        this.location.replaceState('/');
        this.router.navigate(['/login']);
      }
    });
  }

  ngOnDestroy() {
    if (this.sub != null) {
      this.sub.unsubscribe();
    }
  }
}
