package by.bsu.fpmi.pcs.dto;

/**
 * Created by Alexandra on 29.11.2017.
 */
public class IssueCriteria {

    private int statusId;
    private int projectId;
    private String description;

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
