package by.bsu.pcs_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ProjectsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        FragmentProjects fragmentProjects = new FragmentProjects();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragmentProjects);
        fragmentTransaction.commit();
    }
}
