package by.bsu.pcs_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TaskActivity extends AppCompatActivity {

    private TextView tvDescription;
    private TextView tvUserName;
    private TextView tvId;
    private TextView tvStatus;
    private TextView tvPriority;
    private TextView tvStartDate;
    private LinearLayout llColoredLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        tvDescription = (TextView) findViewById(R.id.descriptionTB);
        tvUserName = (TextView) findViewById(R.id.userNameTB);
        tvId = (TextView) findViewById(R.id.idTB);
        tvStatus = (TextView) findViewById(R.id.statusTB);
        tvPriority = (TextView) findViewById(R.id.priorityTB);
        tvStartDate = (TextView) findViewById(R.id.startDateTB);
        llColoredLine = (LinearLayout) findViewById(R.id.colored_line);

        Bundle extras = getIntent().getExtras();
        String description = "";
        String userName = "";
        String id = "";
        String status = "";
        String priority = "";
        String startDate = "";
        int color = 0;

        if (extras != null) {
            description = extras.getString("description");
            userName = extras.getString("userName");
            id = extras.getString("id");
            status = extras.getString("status");
            priority = extras.getString("priority");
            startDate = extras.getString("startDate");
            color = Integer.parseInt(extras.getString("color"));
        }

        if(userName.equals("null")) {
            userName = "Unpicked task.";
        }

        if(startDate.equals("null")) {
            startDate = "Not set.";
        }

        tvDescription.setText(description);
        tvUserName.setText(userName);
        tvId.setText(id);
        tvStatus.setText(status);
        tvPriority.setText(priority);
        tvStartDate.setText(startDate);
        llColoredLine.setBackgroundColor(color);
    }
}
