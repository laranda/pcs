import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { ProjectBlockComponent } from './project-block/project-block.component';
import { CreateProjectBlockComponent } from './create-project-block/create-project-block.component';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [
    CommonModule, SharedModule, FormsModule
  ],
  declarations: [
    ProjectBlockComponent, HomeComponent, CreateProjectBlockComponent
  ],
  exports: [
    ProjectBlockComponent, HomeComponent
  ]
})
export class HomeModule { }
