export const environment = {
  production: true,
  api: {
    url: 'https://secret-oasis-48836.herokuapp.com',
    accessHeader: 'Authorization',
    contType: 'Content-Type',
    contTypeValue: 'application/json'
  },
  auth: {
    sessionKey: 'PCS-user-data'
  }
};
