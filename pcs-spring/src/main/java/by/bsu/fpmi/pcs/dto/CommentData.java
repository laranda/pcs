package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Comment;

import java.text.SimpleDateFormat;

/**
 * Created by Alexandra on 05.12.2017.
 */
public class CommentData {

    private int id;
    private String content;
    private int issueId;
    private String creationTime;
    private int userId;
    private String userName;

    public CommentData() {}

    public CommentData(Comment comment) {
        this.id = comment.getId();
        this.content = comment.getContent();
        this.userId = comment.getUser().getId();
        this.userName = comment.getUser().getName();
        this.issueId = comment.getIssue().getId();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        if (comment.getCreationTime() != null) {
            try {
                this.creationTime = sdf.format(comment.getCreationTime().getTime());
            } catch (Exception e) {
                this.creationTime = null;
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
