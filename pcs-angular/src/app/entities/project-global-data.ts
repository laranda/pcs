/**
 * Created by Alexandra on 29.11.2017.
 */
import { ShortProjectData } from './short-project-data';
import { StatusData } from './status-data';

export class ProjectGlobalData {
  shortProjectData: ShortProjectData;
  statuses: StatusData[];
}
