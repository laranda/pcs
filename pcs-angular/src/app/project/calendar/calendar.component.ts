/**
 * Created by Alexandra on 27.11.2017.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import * as _ from 'lodash';
import { EventService } from "../../services/event.service";
import {EventData} from "../../entities/event-data";
import {ProjectService} from "../../services/project.service";

export interface CalendarDate {
  mDate: moment.Moment;
  selected?: boolean;
  today?: boolean;
  hasEvents?: boolean;
}

@Component({
  selector: 'calendar',
  templateUrl: './calendar.component.html',
  styleUrls: [ './calendar.component.scss' ]
})
export class CalendarComponent implements OnInit {
  id: number;
  currentDate = moment();
  dayNames = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
  weeks: CalendarDate[][] = [];
  selectedDate: CalendarDate = null;
  eventsData: any = null;
  currentEvent: EventData = null;
  showEvents: EventData[] = [];
  isAdmin: boolean = true;


  constructor(private route: ActivatedRoute, private eventService: EventService, private projectService: ProjectService) {}

  ngOnInit(): void {
    this.getEvents();
    this.generateCalendar();
  }

  getEvents(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.eventService.eventsData.subscribe(es => {
      this.eventsData = es;
      this.generateCalendar();
      if (this.selectedDate) {
        const strDate = this.selectedDate.mDate.format('DD.MM.YYYY');
        this.showEvents = this.eventsData ? this.eventsData[strDate] || [] : [];
      }
    });
    this.eventService.eventData.subscribe(e => this.currentEvent = e);
    this.eventService.getEvents(this.id);
    this.eventService.deleted.subscribe(del => { if (del) this.showEventForm(false)});
    this.isAdmin = this.projectService.currentProject.getValue() ? this.projectService.currentProject.getValue().shortProjectData.isAdmin  : true;
    this.projectService.currentProject.subscribe(val => {
      this.isAdmin = val && val.shortProjectData ? val.shortProjectData.isAdmin : true;
    });
  }

  isToday(date: moment.Moment): boolean {
    return moment().isSame(moment(date), 'day');
  }

  isSelected(date: moment.Moment): boolean {
    return moment(date).isSame(this.selectedDate ? this.selectedDate.mDate : null, 'day');
  }

  hasEvents(date: moment.Moment): boolean {
    const strDate = date.format('DD.MM.YYYY');
    return this.eventsData ? !!this.eventsData[strDate] : false;
  }

  isSelectedMonth(date: moment.Moment): boolean {
    return moment(date).isSame(this.currentDate, 'month');
  }

  selectDate(date: CalendarDate): void {
    this.selectedDate = date;
    const strDate = date.mDate.format('DD.MM.YYYY');
    this.showEvents = this.eventsData ? this.eventsData[strDate] || [] : [];
    this.generateCalendar();
  }

  prevMonth(): void {
    this.currentDate = moment(this.currentDate).subtract(1, 'months');
    this.generateCalendar();
  }

  nextMonth(): void {
    this.currentDate = moment(this.currentDate).add(1, 'months');
    this.generateCalendar();
  }

  prevYear(): void {
    this.currentDate = moment(this.currentDate).subtract(1, 'year');
    this.generateCalendar();
  }

  nextYear(): void {
    this.currentDate = moment(this.currentDate).add(1, 'year');
    this.generateCalendar();
  }

  generateCalendar(): void {
    const dates = this.fillDates(this.currentDate);
    const weeks: CalendarDate[][] = [];
    while (dates.length > 0) {
      weeks.push(dates.splice(0, 7));
    }
    this.weeks = weeks;
  }

  fillDates(currentMoment: moment.Moment): CalendarDate[] {
    const firstOfMonth = moment(currentMoment).startOf('month').day();
    const firstDayOfGrid = moment(currentMoment).startOf('month').subtract(firstOfMonth, 'days');
    const start = firstDayOfGrid.date();
    return _.range(start, start + 42)
      .map((date: number): CalendarDate => {
      const d = moment(firstDayOfGrid).date(date);
        return {
          today: this.isToday(d),
          selected: this.isSelected(d),
          mDate: d,
          hasEvents: this.hasEvents(d)
        };
    });
  }

  showEventForm(show: boolean, event: EventData = new EventData()) {
    if (show) {
      this.currentEvent = event;
    } else {
      this.currentEvent = null;
    }
  }

  onSaveEvent() {
    this.currentEvent.date = this.selectedDate.mDate.format('DD.MM.YYYY');
    this.eventService.saveEvent(this.currentEvent, this.id);
  }

  onEventDelete() {
    this.eventService.deleteEvent(this.currentEvent.id, this.id);
  }
}
