package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Role;

/**
 * Created by Alexandra on 07.12.2017.
 */
public class RoleData {
    private int id;
    private String name;
    private boolean isAdmin;

    public RoleData() {}
    public RoleData(Role role) {
        this.id = role.getId();
        this.name = role.getName();
        this.isAdmin = role.isAdmin();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
