/**
 * Created by Alexandra on 26.11.2017.
 */
import { Component, OnInit } from '@angular/core';
import { NotificationData } from "../entities/notification-data";
import { EventService } from "../services/event.service";


@Component({
  selector: 'my-profile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.scss']
})
export class MyProfileComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {
  }
}
