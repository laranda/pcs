package by.bsu.fpmi.pcs.entity;

import javax.persistence.*;

/**
 * Created by Alexandra on 12.11.2017.
 */
@Entity
@Table(name = "token")
public class Token {

    @Id
    @Column(name = "`access_token`")
    private String accessToken;

    @ManyToOne
    @JoinColumn(name = "`user_id`")
    private User user;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
