/**
 * Created by Alexandra on 26.11.2017.
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ExceptionData } from "../entities/exception-data";
import { Router } from "@angular/router";

const ERROR_MESSAGES = {

  // Auth exceptions
  1: 'Use with this email already exists',
  2: 'No user with this email',
  3: 'Invalid email or password',
  4: 'You need to log in to do this',

  //Project exceptions
  100: 'Project was not found',
  101: 'You need admin rights to do this',
  102: 'This user is already a member',
  103: 'This user is not a member of this project',

  //Status exceptions
  200: 'Cannot find the status provided',

  //Issue exceptions
  300: 'Cannot find this issue',

  //User exceptions
  400: 'No such user',

  //Role exceptions
  500: 'No such role',

  //Calendar exceptions
  600: 'No such event',
  601: 'You have provided event time in an illegal format',

  5000: 'Internal server error, sorry',
  10000: 'Unknown server error'
};

@Injectable()
export class ErrorService {

  errorMessage: Subject<string> = new Subject();
  currentErrorId: number = 0;

  constructor(private router: Router) {}

  showError(message: string) {
    this.errorMessage.next(message);
    const newId = ++this.currentErrorId;
    setTimeout(() => {
      if (newId == this.currentErrorId) {
        this.errorMessage.next(null);
      }
    }, 10000);
  }

  showHttpCodeError(err) {
    console.log(err);
  }

  showExceptionData(exception: ExceptionData) {
    if (exception.code === 100) {
      this.router.navigate(['/']);
    }
    this.showError(ERROR_MESSAGES[exception.code] || ERROR_MESSAGES[10000]);
  }

  hideErrorMessage() {
    this.errorMessage.next(null);
  }
}
