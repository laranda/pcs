package by.bsu.pcs_android.dto;

import java.util.List;

public class ProjectGlobalData {
    private ShortProjectData shortProjectData;
    private List<StatusData> statuses;

    public ShortProjectData getShortProjectData() {
        return shortProjectData;
    }

    public void setShortProjectData(ShortProjectData shortProjectData) {
        this.shortProjectData = shortProjectData;
    }

    public List<StatusData> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<StatusData> statuses) {
        this.statuses = statuses;
    }
}
