package by.bsu.pcs_android;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import by.bsu.pcs_android.util.AccountData;
import by.bsu.pcs_android.util.UserData;

/**
 * Created by Koupi on 06.12.2017.
 */

public class RegisterActivity extends AppCompatActivity {

    private RegisterTask mRegisterTask = null;

    private EditText mEmailView;
    private EditText mPasswordView;
    private EditText mConfirmPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mConfirmPasswordView = (EditText) findViewById(R.id.confirm_password);

        Button mRegisterButton = (Button) findViewById(R.id.register_button);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });
    }

    public void showPrActivity() {
        Intent intent = new Intent(this, ProjectsActivity.class);
        startActivity(intent);
    }

    private void attemptRegister() {
        if (mRegisterTask != null) {
            return;
        }

        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String confirmPassword = mConfirmPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(confirmPassword) && !isPasswordValid(confirmPassword)) {
            mConfirmPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if(!confirmPassword.equals(password)) {
            mConfirmPasswordView.setError(getString(R.string.different_passwords));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            mRegisterTask = new RegisterTask(email, password);
            mRegisterTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        return true;
    }

    private boolean isPasswordValid(String password) {
        return true;
    }

    public class RegisterTask extends AsyncTask<Void, Void, Boolean> {
        private final String loginReq;

        public RegisterTask(String email, String password) {
            JSONObject reqJson = new JSONObject();
            try {
                reqJson.put("email", email);
                reqJson.put("password", password);
            } catch (JSONException e) {}
            loginReq = reqJson.toString();
        }


        @Override
        protected Boolean doInBackground(Void... voids) {
            String requestURL = AccountData.ipAddress + "/api/auth/register";
            URL url;
            try {
                url = new URL(requestURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(loginReq);

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                System.out.println("CODE " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    StringBuilder response = new StringBuilder();
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                    JSONObject jsonResponse = new JSONObject(response.toString());
                    System.out.println(response.toString());
                    if (jsonResponse.isNull("exceptionData")) {
                        JSONObject requestedData = jsonResponse.getJSONObject("requestedData");
                        AccountData.accessToken = requestedData.optString("accessToken", "");
                        UserData userData = new UserData(requestedData.getJSONObject("userData"));
                        AccountData.userData = userData;
                        return true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mRegisterTask = null;

            if (success) {
                showPrActivity();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }
    }
}
