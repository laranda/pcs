/**
 * Created by Alexandra on 28.11.2017.
 */

export class ProjectInfoData {
  id: number;
  name: string;
  description: string;
}
