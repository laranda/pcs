import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { BoardComponent } from './board.component';
import { IssueBlockComponent } from './issue-block/issue-block.component';
import { CreateIssueBlockComponent } from './create-issue-block/create-issue-block.component';
import { UpdateIssueBlockComponent } from './update-issue-block/update-issue-block.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    BoardComponent, IssueBlockComponent, CreateIssueBlockComponent, UpdateIssueBlockComponent
  ],
  exports: [
    BoardComponent
  ]
})
export class BoardModule { }
