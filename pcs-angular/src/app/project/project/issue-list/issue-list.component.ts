/**
 * Created by Alexandra on 27.11.2017.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../../../services/project.service';
import { IssueService } from '../../../services/issue.service';
import { IssueCriteria } from '../../../entities/issue-criteria';
import { IssueData } from '../../../entities/issue-data';
import { StatusData } from '../../../entities/status-data';
import {CommentData} from "../../../entities/comment-data";

@Component({
  selector: 'issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: [ './issue-list.component.scss' ]
})
export class IssueListComponent implements OnInit {
  id: number;
  issueCriteria: IssueCriteria = new IssueCriteria();
  statuses: StatusData[] = [];
  issues: IssueData[] = [];
  comments: CommentData[] = [];
  isAdmin: boolean = true;

  constructor(private route: ActivatedRoute, private projectService: ProjectService, private issueService: IssueService) {}

  ngOnInit(): void {
    this.setParams();
  }

  setParams(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.issueCriteria.projectId = this.id;
    this.statuses = this.projectService.currentProject.getValue() ? this.projectService.currentProject.getValue().statuses : [];
    this.isAdmin = this.projectService.currentProject.getValue() ? this.projectService.currentProject.getValue().shortProjectData.isAdmin  : true;
    if (this.statuses.length > 0) {
      this.issueCriteria.statusId = this.statuses[0].id;
    }
    this.projectService.currentProject.subscribe(val => {
      this.statuses = val ? val.statuses : [];
      if (this.statuses.length > 0) {
        this.issueCriteria.statusId = this.statuses[0].id;
      }
      this.isAdmin = val && val.shortProjectData ? val.shortProjectData.isAdmin : true;
    });
    this.issueService.listData.subscribe(val => {
      this.issues = val && val.issues ? val.issues : [];
      if (this.issues.length > 0) {
        this.issueService.getComments(this.issues[0].id);
      }
    });
    this.issueService.commentsData.subscribe(cs => this.comments = cs);
  }

  searchIssues() {
    this.issueService.getIssueList(this.issueCriteria);
  }

  getIssueType(issue: IssueData) {
    return issue.type == 1 ? 'red' : 'blue';
  }
}
