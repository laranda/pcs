package by.bsu.fpmi.pcs.repository;

import by.bsu.fpmi.pcs.entity.Membership;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alexandra on 17.09.2017.
 */
public interface MembershipRepository extends JpaRepository<Membership, Integer> {

    List<Membership> findByUserId(int userId);
    List<Membership> findByProjectId(int projectId);
    Membership findByUserIdAndProjectId(int userId, int projectId);

}
