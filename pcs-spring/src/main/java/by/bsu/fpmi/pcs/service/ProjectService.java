package by.bsu.fpmi.pcs.service;

import by.bsu.fpmi.pcs.dto.*;
import by.bsu.fpmi.pcs.entity.*;
import by.bsu.fpmi.pcs.exception.ExceptionCodes;
import by.bsu.fpmi.pcs.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Alexandra on 22.11.2017.
 */
@Service
public class ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    MembershipRepository membershipRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    UserRepository userRepository;


    public List<ShortProjectData> getUserProjects(User user) throws ServiceException {
        try {
            List<Membership> membs = membershipRepository.findByUserId(user.getId());
            List<ShortProjectData> data = new ArrayList<>();
            for (Membership membership : membs) {
                ShortProjectData shortProjectData = new ShortProjectData((membership));
                long end = Calendar.getInstance().getTimeInMillis();
                long start = membership.getProject().getCreationDate().getTimeInMillis();
                int days = (int) TimeUnit.MILLISECONDS.toDays(Math.abs(end - start));
                if (days > 30) {
                    shortProjectData.setMonthDuration(days / 30);
                } else {
                    shortProjectData.setDaysDuration(days);
                }
                data.add(shortProjectData);
            }
            return data;
        }  catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get projects for user with id: %d", user.getId()), e);
        }
    }

    public ProjectGlobalData getProjectData(User user, int projectId) throws ServiceException {
        try {
            Membership membership = checkUserRights(user, projectId, false);
            ProjectGlobalData projectGlobalData = new ProjectGlobalData();
            projectGlobalData.setShortProjectData(new ShortProjectData(membership));
            projectGlobalData.setStatuses(statusRepository.findByProjectId(projectId)
                    .stream().map(StatusData::new).collect(Collectors.toList()));
            return projectGlobalData;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get project data with id: %d for user with id: %d", projectId, user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public ShortProjectData createNewProject(User user, ProjectInfoData projectInfoData) throws ServiceException {
        try {
            Project project = new Project();
            project.setName(projectInfoData.getName());
            project.setDescription(projectInfoData.getDescription());
            project.setCreationDate(Calendar.getInstance());
            projectRepository.save(project);
            Role pmRole = roleRepository.findOne(1);
            if (pmRole == null) {
                throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                        String.format("Cannot find PM role"));
            }
            Status queueStatus = new Status();
            queueStatus.setProject(project);
            queueStatus.setName("Queue");
            queueStatus.setOrder(0);
            statusRepository.save(queueStatus);
            Status doneStatus = new Status();
            doneStatus.setProject(project);
            doneStatus.setName("Done");
            doneStatus.setOrder(1);
            statusRepository.save(doneStatus);
            Membership membership = new Membership();
            membership.setUser(user);
            membership.setProject(project);
            membership.setRole(pmRole);
            membershipRepository.save(membership);
            return  new ShortProjectData(membership);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to create new project (name=%s, description=%s) for new user with id: %d",
                            projectInfoData.getName(), projectInfoData.getDescription(), user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public void deleteProject(User user, int projectId) throws ServiceException {
        try {
            Membership membership = checkUserRights(user, projectId, true);
            projectRepository.delete(membership.getProject());
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to delete project (id=%d) for user with id: %d",
                            projectId, user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public ProjectInfoData modifyProject(User user, ProjectInfoData projectInfoData) throws ServiceException {
        try {
            Membership membership = checkUserRights(user, projectInfoData.getId(), true);
            Project project = membership.getProject();
            project.setName(projectInfoData.getName());
            project.setDescription(projectInfoData.getDescription());
            projectRepository.save(project);
            return new ProjectInfoData(project);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to modify project (id=%d) for user with id: %d",
                            projectInfoData.getId(), user.getId()), e);
        }
    }

    public MembershipData saveMembership(User user, int projectId, MembershipData membershipData) throws ServiceException {
        if (membershipData.getId() > 0) {
            return modifyMembership(user, projectId, membershipData);
        }
        return addMember(user, projectId, membershipData);
    }

    @Transactional(rollbackFor = ServiceException.class)
    public MembershipData addMember(User user, int projectId, MembershipData membershipData) throws ServiceException {
        try {
            Membership membership = checkUserRights(user, projectId, true);
            User member;
            if (membershipData.getUserEmail() != null) {
                member = userRepository.findByEmail(membershipData.getUserEmail());
            } else {
                member = userRepository.findOne(membershipData.getUserId());
            }
            if (member == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_USER_EXCEPTION,
                        String.format("Cannot find user with id: %d", membershipData.getUserId()));
            }
            Membership existed = membershipRepository.findByUserIdAndProjectId(member.getId(), projectId);
            if (existed != null) {
                throw new ServiceException(ExceptionCodes.MEMBER_EXISTS_EXCEPTION,
                        String.format("Membership already exists for user with id: %d and project with id: %d", member.getId(), projectId));
            }
            Role role = roleRepository.findOne(membershipData.getRoleId());
            if (role == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_ROLE_EXCEPTION,
                        String.format("Cannot find role with id: %d", membershipData.getRoleId()));
            }
            Membership newMembership = new Membership();
            newMembership.setUser(member);
            newMembership.setProject(membership.getProject());
            newMembership.setRole(role);
            membershipRepository.save(newMembership);
            return new MembershipData(newMembership);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to add new member to project (id=%d) for user with id: %d",
                            projectId, user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public MembershipData modifyMembership(User user, int projectId, MembershipData membershipData) throws ServiceException {
        try {
            checkUserRights(user, projectId, true);
            User member = userRepository.findOne(membershipData.getUserId());
            if (member == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_USER_EXCEPTION,
                        String.format("Cannot find user with id: %d", membershipData.getUserId()));
            }
            Membership membership = membershipRepository.findOne(membershipData.getId());
            if (membership == null || membership.getUser().getId() != membershipData.getUserId() || membership.getProject().getId() != membershipData.getProjectId()) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_MEMBERSHIP_EXCEPTION,
                        String.format("Membership not found with id: %d for user with id: %d and project with id: %d", membershipData.getId(), membershipData.getUserId(), membershipData.getProjectId()));
            }
            Role role = roleRepository.findOne(membershipData.getRoleId());
            if (role == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_ROLE_EXCEPTION,
                        String.format("Cannot find role with id: %d", membershipData.getRoleId()));
            }
            membership.setRole(role);
            membershipRepository.save(membership);
            return new MembershipData(membership);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to modify membership for project (id=%d) for user with id: %d",
                            projectId, user.getId()), e);
        }
    }

    public List<MembershipData> getMembers(User user, int projectId) throws ServiceException {
        try {
            Membership membership = checkUserRights(user, projectId, false);
            List<MembershipData> memberships = membershipRepository.findByProjectId(projectId)
                    .stream().map(MembershipData::new).collect(Collectors.toList());
            return memberships;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get memberships for project (id=%d) for user with id: %d",
                            projectId, user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public StatusData addStatus(User user, int projectId, StatusData statusData) throws ServiceException {
        try {
            Membership membership = checkUserRights(user, projectId, true);
            List<Status> statuses = statusRepository.findByProjectId(projectId);
            if (statuses.size() <= statusData.getOrder() || statusData.getOrder() == 0) {
                return null;
            }
            for (Status status : statuses) {
                if (status.getOrder() >= statusData.getOrder()) {
                    status.setOrder(status.getOrder() + 1);
                    statusRepository.save(status);
                }
            }
            Status status = new Status();
            status.setName(statusData.getName());
            status.setOrder(statusData.getOrder());
            status.setProject(membership.getProject());
            statusRepository.save(status);
            return new StatusData(status);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to add new status to project (id=%d) for user with id: %d",
                            projectId, user.getId()), e);
        }
    }

    Membership checkUserRights(User user, int projectId, boolean needAdmin) throws ServiceException {
        try {
            Membership membership = membershipRepository.findByUserIdAndProjectId(user.getId(), projectId);
            if (membership == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_PROJECT_EXCEPTION,
                        String.format("User (id=%d) has no project with id: %d", user.getId(), projectId));
            }
            if (!needAdmin) {
                return membership;
            }
            if (!membership.getRole().isAdmin()) {
                throw new ServiceException(ExceptionCodes.NO_ADMIN_RIGHT_EXCEPTION,
                        String.format("User (id=%d) has no admin rights to project with id: %d", user.getId(), projectId));
            }
            return membership;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to check user rights for project (id=%d) for user with id: %d",
                            projectId, user.getId()), e);
        }
    }

}
