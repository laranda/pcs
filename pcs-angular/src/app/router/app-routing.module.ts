import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DemoComponent } from '../demo/demo.component';

import { LoginComponent } from '../auth/login/login.component';
import { HomeComponent } from '../home/home.component';
import { RegisterComponent } from "../auth/register/register.component";

import { BoardComponent } from '../project/project/board/board.component';
import { TeamComponent } from '../project/project/team/team.component';
import { IssueListComponent } from '../project/project/issue-list/issue-list.component';
import { SettingsComponent } from '../project/project/settings/settings.component';
import { CalendarComponent } from '../project/calendar/calendar.component';
import { AnalyticsComponent } from '../project/analytics/analytics.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { MyProfileComponent } from '../myprofile/myprofile.component';


const routes: Routes = [
  { path: 'demo', component: DemoComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'notifications', component: NotificationsComponent },
  { path: 'myprofile', component: MyProfileComponent },
  { path: 'project/:id', redirectTo: 'project/:id/board', pathMatch: 'full' },
  { path: 'project/:id/board', component: BoardComponent },
  { path: 'project/:id/team', component: TeamComponent },
  { path: 'project/:id/settings', component: SettingsComponent },
  { path: 'project/:id/issue-list', component: IssueListComponent },
  { path: 'project/:id/calendar', component: CalendarComponent },
  { path: 'project/:id/analytics', component: AnalyticsComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: HomeComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule { }
