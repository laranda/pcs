/**
 * Created by Alexandra on 28.11.2017.
 */

export class IssueData {
  id: number;
  description: string;
  priority: number;
  statusId: number;
  statusName: string;
  projectId: number;
  userId: number;
  userName: string;
  startDate: string;
  endDate: string;
  type: number;
}
