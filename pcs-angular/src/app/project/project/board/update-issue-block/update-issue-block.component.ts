import {Component, OnInit, OnChanges, Input, SimpleChanges} from '@angular/core';
import { IssueData } from "../../../../entities/issue-data";
import { IssueType, types } from "../../../../entities/issue-types";
import { IssuePriority, priorities } from "../../../../entities/issue-priorities";
import { StatusData } from "../../../../entities/status-data";
import { ProjectService } from "../../../../services/project.service";
import { IssueService } from "../../../../services/issue.service";
import { CommentData } from "../../../../entities/comment-data";
import { MembershipData } from "../../../../entities/membership-data";


@Component({
  selector: 'update-issue-block',
  templateUrl: './update-issue-block.component.html',
  styleUrls: ['./update-issue-block.component.scss']
})
export class UpdateIssueBlockComponent implements OnInit, OnChanges {
  @Input() issue: IssueData;
  types: IssueType[] = types;
  priorities: IssuePriority[] = priorities;
  @Input() cancel: Function;
  statuses: StatusData[] = [];
  comment: CommentData = new CommentData();
  comments: CommentData[] = [];
  members: MembershipData[] = [];
  noAssigneeMembership: MembershipData = new MembershipData();

  constructor(private projectService: ProjectService, private issueService: IssueService) {
    this.noAssigneeMembership.userName = 'No assignee';
    this.noAssigneeMembership.userId = 0;
    if (projectService.currentProject.getValue() && projectService.currentProject.getValue().statuses) {
      this.statuses = projectService.currentProject.getValue().statuses;
    }
    this.members = [...projectService.members.getValue(), this.noAssigneeMembership];
    this.projectService.currentProject.subscribe(pr => {
      this.statuses = pr && pr.statuses ? pr.statuses : [];
    });
    this.issueService.commentsData.subscribe(cs => this.comments = cs);
    projectService.members.subscribe(ms => {
      this.members = [this.noAssigneeMembership];
      this.members.push(...ms);
    });
    this.issueService.deleted.subscribe(del => {if (del) this.cancel(false)});
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.issueService.getComments(changes.issue.currentValue.id);
  }

  onCancel() {
    this.cancel(false);
  }

  onDelete() {
    this.issueService.deleteIssue(this.issue.id, this.issue.projectId);
  }

  onTypeSelect(type) {
    this.issue.type = type;
  }
  onPrioritySelect(pr) {
    this.issue.priority = pr;
  }
  onStatusSelect(st) {
    this.issue.statusId = st;
  }
  onAssigneeSelect(user) {
    this.issue.userId = user;
  }

  onSave() {
    this.issueService.saveIssue(this.issue);
  }

  addComment() {
    this.issueService.addComment(this.issue.id, this.comment);
  }
}
