package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Status;

import java.util.List;

/**
 * Created by Alexandra on 28.11.2017.
 */
public class StatusData {
    private int id;
    private String name;
    private int order;
    private List<IssueData> issues;

    public StatusData() {}
    public StatusData(Status status) {
        this.id = status.getId();
        this.name = status.getName();
        this.order = status.getOrder();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<IssueData> getIssues() {
        return issues;
    }

    public void setIssues(List<IssueData> issues) {
        this.issues = issues;
    }
}
