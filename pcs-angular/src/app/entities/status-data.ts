/**
 * Created by Alexandra on 28.11.2017.
 */
import { IssueData } from './issue-data';

export class StatusData {
  id: number;
  name: string;
  order: number;
  issues: IssueData[];
}
