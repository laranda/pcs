package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.exception.PCSException;

/**
 * Created by Alexandra on 18.11.2017.
 */
public class ExceptionData {

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public ExceptionData() {}

    public ExceptionData(PCSException e) {
        this.code = e.getCode();
        this.message = e.getMessage();
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
