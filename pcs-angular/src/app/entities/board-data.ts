/**
 * Created by Alexandra on 28.11.2017.
 */
import { IssueData } from './issue-data';
import { StatusData } from './status-data';

export class BoardData {
  goals: IssueData[];
  expedite: StatusData[];
  issues: StatusData[];
}
