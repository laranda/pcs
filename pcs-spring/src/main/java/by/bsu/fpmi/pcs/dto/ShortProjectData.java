package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Membership;

/**
 * Created by Alexandra on 22.11.2017.
 */
public class ShortProjectData {

    private int id;
    private String name;
    private String description;
    private String roleName;
    private boolean isAdmin;
    private int monthDuration;
    private int daysDuration;

    public ShortProjectData() {}

    public ShortProjectData(Membership membership) {
        this.id = membership.getProject().getId();
        this.name = membership.getProject().getName();
        this.description = membership.getProject().getDescription();
        this.roleName = membership.getRole().getName();
        this.isAdmin = membership.getRole().isAdmin();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getMonthDuration() {
        return monthDuration;
    }

    public void setMonthDuration(int monthDuration) {
        this.monthDuration = monthDuration;
    }

    public int getDaysDuration() {
        return daysDuration;
    }

    public void setDaysDuration(int daysDuration) {
        this.daysDuration = daysDuration;
    }

}
