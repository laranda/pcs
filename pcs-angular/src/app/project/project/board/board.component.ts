/**
 * Created by Alexandra on 27.11.2017.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IssueService } from '../../../services/issue.service';
import { BoardData } from '../../../entities/board-data';
import { IssueData } from "../../../entities/issue-data";
import { ProjectService } from "../../../services/project.service";

@Component({
  selector: 'board',
  templateUrl: './board.component.html',
  styleUrls: [ './board.component.scss' ]
})
export class BoardComponent implements OnInit {
  id: number;
  data: BoardData = null;
  subIssues: any = null;
  showForm: boolean = false;
  showIssue: IssueData = null;
  isAdmin: boolean = true;

  constructor(private route: ActivatedRoute, private issueService: IssueService, private projectService: ProjectService) {
    this.setShowForm = this.setShowForm.bind(this);
    this.cancelUpdateForm = this.cancelUpdateForm.bind(this);
    this.isAdmin = projectService.currentProject.getValue() ? projectService.currentProject.getValue().shortProjectData.isAdmin : true;
  }

  ngOnInit(): void {
    this.subIssues = this.issueService.boardData.subscribe(data => this.data = data);
    this.route.params.subscribe(() => this.getIssues());
    this.issueService.created.subscribe(created => this.setShowForm(!created));
    this.issueService.issueData.subscribe(issue => {
      if (!!this.showIssue) {
        this.showIssue = issue;
      }
    });
    this.getIssues();
    this.projectService.currentProject.subscribe(p => {
      this.isAdmin = p ? p.shortProjectData.isAdmin : true;
    });
  }

  getIssues(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.issueService.getBoardData(this.id);
    this.projectService.getMembers(this.id);
  }

  setShowForm(show: boolean) {
    this.showForm = show;
  }

  cancelUpdateForm() {
    this.showIssue = null;
  }

  openUpdateForm(issue: IssueData) {
    this.showIssue = issue;
  }
}
