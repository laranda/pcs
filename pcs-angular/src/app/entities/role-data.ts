/**
 * Created by Alexandra on 07.12.2017.
 */

export class RoleData {
  id: number;
  name: string;
  admin: boolean;
}
