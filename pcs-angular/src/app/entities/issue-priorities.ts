/**
 * Created by Alexandra on 07.12.2017.
 */

export class IssuePriority {
  id: number;
  name: string;
}

export const priorities: IssuePriority[] = [
  {id: 0, name: 'Trivial'},
  {id: 1, name: 'Minor'},
  {id: 2, name: 'Major'},
  {id: 3, name: 'Critical'},
  {id: 4, name: 'Blocker'},
];
