import{ Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'secondary-project-nav',
  templateUrl: './secondary-project-nav.component.html',
  styleUrls: ['./secondary-project-nav.component.scss']
})
export class SecondaryProjectNavComponent implements OnInit {
  @Input() projectId: number;
  @Input() isAdmin: boolean;

  constructor() {}

  ngOnInit(): void {
  }
}
