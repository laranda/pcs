import{ Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'stripe-block',
  templateUrl: './stripe-block.component.html',
  styleUrls: ['./stripe-block.component.scss']
})
export class StripeBlockComponent implements OnInit {
  @Input() color: string = 'red';

  constructor() {}

  ngOnInit(): void {
  }
}
