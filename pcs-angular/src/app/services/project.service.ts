/**
 * Created by Alexandra on 26.11.2017.
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ShortProjectData } from '../entities/short-project-data';
import { ProjectInfoData } from '../entities/project-info-data';
import { ProjectGlobalData } from '../entities/project-global-data';
import { ResponseData } from '../entities/response-data';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';
import { StatusData } from "../entities/status-data";
import { MembershipData } from "../entities/membership-data";
import {ErrorService} from "./error.service";

@Injectable()
export class ProjectService {

  private projectsUrl = `${environment.api.url}/api/projects`;

  projectsData: BehaviorSubject<ShortProjectData[]> = new BehaviorSubject([]);
  currentProject: BehaviorSubject<ProjectGlobalData> = new BehaviorSubject(null);
  members: BehaviorSubject<MembershipData[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient, private authService: AuthService, private errorService: ErrorService){}

  getProjects() {
    this.http.get(this.projectsUrl, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.projectsData.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  getProject(projectId: number) {
    this.http.get(`${this.projectsUrl}/${projectId}`, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.currentProject.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  updateCurrentProject(newId: number) {
    if (!newId) {
      this.currentProject.next(null);
      return;
    }
    if (!!this.currentProject.getValue() && this.currentProject.getValue().shortProjectData.id === newId) {
      return;
    }
    this.getProject(newId);
  }

  createProject(projectInfoData: ProjectInfoData) {
    const prId = projectInfoData.id;
    this.http.post(this.projectsUrl, projectInfoData, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.getProjects();
            if (prId) {
              this.getProject(prId);
            }
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  createStatus(projectId: number, statusData: StatusData) {
    this.http.post(`${this.projectsUrl}/${projectId}/statuses`, statusData, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.getProjects();
            this.getProject(projectId);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  getMembers(projectId: number) {
    this.http.get(`${this.projectsUrl}/${projectId}/members`, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.members.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  addMember(projectId: number, membershipData: MembershipData) {
    this.http.post(`${this.projectsUrl}/${projectId}/members`, membershipData, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.getMembers(projectId);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

}

