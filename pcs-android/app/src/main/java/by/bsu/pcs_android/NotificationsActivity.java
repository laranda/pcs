package by.bsu.pcs_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NotificationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        FragmentNotification fragmentNotifications = new FragmentNotification();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragmentNotifications);
        fragmentTransaction.commit();
    }
}
