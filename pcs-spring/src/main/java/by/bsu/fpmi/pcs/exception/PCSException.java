package by.bsu.fpmi.pcs.exception;

/**
 * Created by Alexandra on 16.11.2017.
 */
public abstract class PCSException extends Exception {

    private int code;

    public PCSException(int code) {
        super();
        this.code = code;
    }

    public PCSException(int code, String message) {
        super(message);
        this.code = code;
    }

    public PCSException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public PCSException(int code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
