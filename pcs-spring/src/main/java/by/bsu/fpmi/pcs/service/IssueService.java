package by.bsu.fpmi.pcs.service;

import by.bsu.fpmi.pcs.dto.*;
import by.bsu.fpmi.pcs.entity.*;
import by.bsu.fpmi.pcs.exception.ExceptionCodes;
import by.bsu.fpmi.pcs.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Alexandra on 22.11.2017.
 */
@Service
public class IssueService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    MembershipRepository membershipRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    IssueRepository issueRepository;

    @Autowired
    ProjectService projectService;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CommentRepository commentRepository;


    public BoardData getBoardIssues(User user, int projectId) throws ServiceException {
        try {
            projectService.checkUserRights(user, projectId, false);
            BoardData boardData = new BoardData();
            boardData.setGoals(issueRepository.findByProjectIdAndType(projectId, IssueType.GOAL)
                    .stream().map(IssueData::new).collect(Collectors.toList()));
            boardData.setExpedite(new ArrayList<>());
            boardData.setIssues(new ArrayList<>());
            List<Status> statuses = statusRepository.findByProjectId(projectId);
            statuses.sort((s1, s2) -> Integer.compare(s1.getOrder(), s2.getOrder()));
            for (Status status : statuses) {
                List<Issue> issues = issueRepository.findByProjectIdAndStatusId(projectId, status.getId());
                StatusData expedite = new StatusData(status);
                StatusData usual = new StatusData(status);
                expedite.setIssues(issues.stream().filter(i -> i.getType() == IssueType.EXPEDITE)
                        .map(IssueData::new).collect(Collectors.toList()));
                usual.setIssues(issues.stream().filter(i -> i.getType() == IssueType.TASK || i.getType() == IssueType.BUG)
                        .map(IssueData::new).collect(Collectors.toList()));
                boardData.getExpedite().add(expedite);
                boardData.getIssues().add(usual);
            }
            return boardData;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get board issues for user with id: %d", user.getId()), e);
        }
    }

    public StatusData getIssuesList(User user, IssueCriteria issueCriteria) throws ServiceException {
        try {
            projectService.checkUserRights(user, issueCriteria.getProjectId(), false);
            Status status = statusRepository.findByIdAndProjectId(issueCriteria.getStatusId(), issueCriteria.getProjectId());
            if (status == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_STATUS_EXCEPTION,
                        String.format("No status with id: %d for project with id: %d", issueCriteria.getStatusId(), issueCriteria.getProjectId()));
            }
            StatusData statusData = new StatusData(status);
            if (issueCriteria.getDescription() == null) {
                statusData.setIssues(issueRepository.findByProjectIdAndStatusId(
                        issueCriteria.getProjectId(), issueCriteria.getStatusId())
                        .stream().map(IssueData::new).collect(Collectors.toList()));
            } else {
                statusData.setIssues(issueRepository.findByProjectIdAndStatusIdAndDescriptionIgnoreCaseContaining(
                        issueCriteria.getProjectId(), issueCriteria.getStatusId(), issueCriteria.getDescription())
                        .stream().map(IssueData::new).collect(Collectors.toList()));
            }
            statusData.getIssues().sort((i1, i2) -> Integer.compare(i1.getType(), i2.getType()));
            return statusData;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get list issues for user with id: %d", user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public void deleteIssue(User user, int issueId) throws ServiceException {
        try {
            Issue issue = issueRepository.findOne(issueId);
            if (issue == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_ISSUE_EXCEPTION,
                        String.format("No issue with id: %d", issueId));
            }
            projectService.checkUserRights(user, issue.getProject().getId(), true);
            issueRepository.delete(issue);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to delete issue with id: %d for user with id: %d", issueId, user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public IssueData saveIssue(User user, IssueData issueData) throws ServiceException {
        if (issueData.getId() > 0) {
            return modifyIssue(user, issueData);
        }
        return createIssue(user, issueData);
    }

    @Transactional(rollbackFor = ServiceException.class)
    public IssueData createIssue(User user, IssueData issueData) throws ServiceException {
        try {
            Membership membership = projectService.checkUserRights(user, issueData.getProjectId(), false);
            Issue issue = new Issue();
            issue.setDescription(issueData.getDescription());
            issue.setPriority(issueData.getPriority());
            issue.setType(issueData.getType());
            issue.setProject(membership.getProject());
            setIssueStatus(issue, issueData);
            setIssueUser(issue, issueData);
            issueRepository.save(issue);
            return new IssueData(issue);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to create new issue for user with id: %d", user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public IssueData modifyIssue(User user, IssueData issueData) throws ServiceException {
        try {
            projectService.checkUserRights(user, issueData.getProjectId(), false);
            Issue issue = issueRepository.findOne(issueData.getId());
            if (issue == null || issue.getProject().getId() != issueData.getProjectId()) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_ISSUE_EXCEPTION,
                        String.format("No issue with id: %d for project with id: %d", issueData.getId(), issueData.getProjectId()));
            }
            issue.setDescription(issueData.getDescription());
            issue.setPriority(issueData.getPriority());
            issue.setType(issueData.getType());
            setIssueStatus(issue, issueData);
            setIssueUser(issue, issueData);
            issueRepository.save(issue);
            return new IssueData(issue);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to update issue with id: %d for user with id: %d", issueData.getId(), user.getId()), e);
        }
    }

    public IssueData getIssue(User user, int issueId) throws ServiceException {
        try {
            Issue issue = issueRepository.findOne(issueId);
            if (issue == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_ISSUE_EXCEPTION,
                        String.format("No issue with id: %d", issueId));
            }
            projectService.checkUserRights(user, issue.getProject().getId(), false);
            return new IssueData(issue);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get issue with id: %d for user with id: %d", issueId, user.getId()), e);
        }
    }

    public List<CommentData> getComments(User user, int issueId) throws ServiceException {
        try {
            Issue issue = issueRepository.findOne(issueId);
            if (issue == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_ISSUE_EXCEPTION,
                        String.format("No issue with id: %d", issueId));
            }
            projectService.checkUserRights(user, issue.getProject().getId(), false);
            List<CommentData> comments = commentRepository.findByIssueId(issueId)
                    .stream().map(CommentData::new).collect(Collectors.toList());
            return comments;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get comment of issue with id: %d for user with id: %d", issueId, user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public CommentData addComment(User user, int issueId, CommentData commentData) throws ServiceException {
        try {
            Issue issue = issueRepository.findOne(issueId);
            if (issue == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_ISSUE_EXCEPTION,
                        String.format("No issue with id: %d", issueId));
            }
            projectService.checkUserRights(user, issue.getProject().getId(), false);
            Comment comment = new Comment();
            comment.setUser(user);
            comment.setIssue(issue);
            comment.setContent(commentData.getContent());
            comment.setCreationTime(Calendar.getInstance());
            commentRepository.save(comment);
            return new CommentData(comment);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to create comment of issue with id: %d for user with id: %d", issueId, user.getId()), e);
        }
    }

    private void setIssueStatus(Issue issue, IssueData issueData) throws ServiceException {
        if (issueData.getType() > 0 && issueData.getStatusId() > 0) {
            Status status = statusRepository.findByIdAndProjectId(issueData.getStatusId(), issueData.getProjectId());
            if (status == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_STATUS_EXCEPTION,
                        String.format("No status with id: %d for project with id: %d", issueData.getStatusId(), issueData.getProjectId()));
            }
            issue.setStatus(status);
        }
    }

    private void setIssueUser(Issue issue, IssueData issueData) throws ServiceException {
        if (issueData.getType() > 0 && issueData.getUserId() > 0) {
            User assignee = userRepository.findOne(issueData.getUserId());
            if (assignee == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_USER_EXCEPTION,
                        String.format("No user with id: %d ", issueData.getUserId()));
            }
            projectService.checkUserRights(assignee, issueData.getProjectId(), false);
            issue.setUser(assignee);
        }
    }

}
