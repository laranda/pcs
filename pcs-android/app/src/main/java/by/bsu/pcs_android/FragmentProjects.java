package by.bsu.pcs_android;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import by.bsu.pcs_android.dto.ShortProjectData;
import by.bsu.pcs_android.util.AccountData;

public class FragmentProjects extends Fragment {

    private LoadProjectsTask mLoadTask = null;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<ShortProjectData> shortProjects = new ArrayList<>();

    public void showTasks(String id) {
        Intent intent = new Intent(this.getContext(), TasksActivity.class);
        intent.putExtra("project_id", id);
        startActivity(intent);
    }

    public class ShortProjectsAdapter extends RecyclerView.Adapter<ShortProjectsAdapter.ViewHolder> {
        private List<ShortProjectData> mDataset;

        public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName, tvDescription;
        public ImageView tvImage;
        public String projectId;

        public ViewHolder(View v) {
                super(v);
                tvName = (TextView) v.findViewById(R.id.nameTB);
                tvDescription = (TextView) v.findViewById(R.id.descriptionTB);
                tvImage = (ImageView) v.findViewById(R.id.iconBox);
                tvImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showTasks(projectId);
                    }
                });
            }
        }

        @Override
        public ShortProjectsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_list_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            ShortProjectData f = mDataset.get(position);
            holder.tvName.setText(f.getName());
            holder.tvDescription.setText(f.getDescription());
            holder.projectId = "" + f.getId();
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }

        public ShortProjectsAdapter(List<ShortProjectData> myDataset) {
            mDataset = myDataset;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_projects, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.search_result);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ShortProjectsAdapter(shortProjects);
        mRecyclerView.setAdapter(mAdapter);

        mLoadTask = new LoadProjectsTask();
        mLoadTask.execute((Void) null);
    }

    public void showProjects(List<ShortProjectData> projects) {
        shortProjects.clear();
        if(projects.size() > 0) {
            shortProjects.addAll(projects);
            mAdapter.notifyDataSetChanged();
            return;
        }
        Toast.makeText(getContext(), "No projects found.", Toast.LENGTH_LONG).show();
    }

    public class LoadProjectsTask extends AsyncTask<Void, Void, List<ShortProjectData>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<ShortProjectData> projects) {
            super.onPostExecute(projects);
            showProjects(projects);
        }

        @Override
        protected List<ShortProjectData> doInBackground(Void... voids) {
            String requestURL = AccountData.ipAddress + "/api/projects";
            URL url;
            try {
                url = new URL(requestURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Authorization", AccountData.accessToken);

                int responseCode = conn.getResponseCode();
                System.out.println("CODE " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    StringBuilder response = new StringBuilder();
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                    JSONObject jsonResponse = new JSONObject(response.toString());
                    System.out.println(response.toString());

                    if (jsonResponse.isNull("exceptionData")) {
                        JSONArray requestedData = jsonResponse.getJSONArray("requestedData");
                        List<ShortProjectData> projects = new ArrayList<>();
                        System.out.println("length = " + requestedData.length());
                        for(int i = 0; i < requestedData.length(); i++) {
                            JSONObject obj = requestedData.getJSONObject(i);
                            System.out.println("obj:\n " + obj.toString());
                            int id = obj.optInt("id", 0);
                            String name = obj.optString("name", "");
                            String description = obj.optString("description", "");
                            String roleName = obj.optString("roleName", "");
                            boolean isAdmin = obj.optBoolean("isAdmin", true);
                            int monthDuration = obj.optInt("monthDuration", 0);
                            int daysDuration = obj.optInt("daysDuration", 0);
                            ShortProjectData pr = new ShortProjectData(id, name, description, roleName, isAdmin, monthDuration, daysDuration);
                            projects.add(pr);
                        }
                        return projects;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ArrayList<>();

        }
    }
}
