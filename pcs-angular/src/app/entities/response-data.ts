/**
 * Created by Alexandra on 26.11.2017.
 */
import { ExceptionData } from './exception-data';

export class ResponseData {
  exceptionData: ExceptionData;
  requestedData: any;
}
