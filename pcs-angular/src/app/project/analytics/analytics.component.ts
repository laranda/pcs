/**
 * Created by Alexandra on 27.11.2017.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'analytics',
  templateUrl: './analytics.component.html',
  styleUrls: [ './analytics.component.scss' ]
})
export class AnalyticsComponent implements OnInit {
  id: number;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.getAnalytics();
  }

  getAnalytics(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    // this.heroService.getHero(id)
    //   .subscribe(hero => this.hero = hero);
  }
}
