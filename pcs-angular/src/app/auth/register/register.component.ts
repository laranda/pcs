import{ Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { LoginData } from '../../entities/login-data';

import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  loginData: LoginData = new LoginData();
  confirmPassword: string;
  authSub: any = null;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/home']);
    }
    this.authSub = this.authService.tokenData.subscribe(value => {
      if (value && value.accessToken) {
        this.router.navigate(['/home']);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.authSub !== null) {
      this.authSub.unsubscribe();
    }
  }

  register() {
    if (this.loginData.password === this.confirmPassword) {
      this.authService.register(this.loginData);
    }
  }
}
