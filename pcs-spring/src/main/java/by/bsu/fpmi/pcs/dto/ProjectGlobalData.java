package by.bsu.fpmi.pcs.dto;

import java.util.List;

/**
 * Created by Alexandra on 29.11.2017.
 */
public class ProjectGlobalData {
    private ShortProjectData shortProjectData;
    private List<StatusData> statuses;

    public ShortProjectData getShortProjectData() {
        return shortProjectData;
    }

    public void setShortProjectData(ShortProjectData shortProjectData) {
        this.shortProjectData = shortProjectData;
    }

    public List<StatusData> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<StatusData> statuses) {
        this.statuses = statuses;
    }
}
