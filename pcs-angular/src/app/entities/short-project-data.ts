/**
 * Created by Alexandra on 25.11.2017.
 */

export class ShortProjectData {
  id: number;
  name: string;
  description: string;
  roleName: string;
  isAdmin: boolean;
  monthDuration: number;
  daysDuration: number;
}
