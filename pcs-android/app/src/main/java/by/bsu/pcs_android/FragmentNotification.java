package by.bsu.pcs_android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import by.bsu.pcs_android.dto.NotificationData;
import by.bsu.pcs_android.dto.ShortProjectData;
import by.bsu.pcs_android.util.AccountData;


public class FragmentNotification extends Fragment {
    private LoadNotificationTask mLoadTask = null;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<NotificationData> notifications = new ArrayList<>();

    public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
        private List<NotificationData> mDataset;

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView tvProject, tvDescription, tvTime;

            public ViewHolder(View v) {
                super(v);
                tvProject = (TextView) v.findViewById(R.id.projectTB);
                tvDescription = (TextView) v.findViewById(R.id.descriptionTB);
                tvTime = (TextView) v.findViewById(R.id.timeTB);

            }
        }

        @Override
        public NotificationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, parent, false);
            NotificationsAdapter.ViewHolder vh = new NotificationsAdapter.ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(NotificationsAdapter.ViewHolder holder, final int position) {
            NotificationData f = mDataset.get(position);
            holder.tvProject.setText(f.getProjectName());
            holder.tvDescription.setText(f.getDescription());
            holder.tvTime.setText(f.getTime());
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }

        public NotificationsAdapter(List<NotificationData> myDataset) {
            mDataset = myDataset;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.search_result);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NotificationsAdapter(notifications);
        mRecyclerView.setAdapter(mAdapter);

        mLoadTask = new LoadNotificationTask();
        mLoadTask.execute((Void) null);
    }

    public void showNotifications(List<NotificationData> notificationsList) {
        notifications.clear();
        if(notificationsList.size() > 0) {
            notifications.addAll(notificationsList);
            mAdapter.notifyDataSetChanged();
            return;
        }
        Toast.makeText(getContext(), "No notifications found.", Toast.LENGTH_LONG).show();
    }

    public class LoadNotificationTask extends AsyncTask<Void, Void, List<NotificationData>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<NotificationData> notifications) {
            super.onPostExecute(notifications);
            showNotifications(notifications);
        }

        @Override
        protected List<NotificationData> doInBackground(Void... voids) {
            String requestURL = AccountData.ipAddress + "/api/notifications";
            URL url;
            try {
                url = new URL(requestURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Authorization", AccountData.accessToken);

                int responseCode = conn.getResponseCode();
                System.out.println("CODE " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    StringBuilder response = new StringBuilder();
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                    JSONObject jsonResponse = new JSONObject(response.toString());
                    System.out.println(response.toString());

                    if (jsonResponse.isNull("exceptionData")) {
                        JSONArray requestedData = jsonResponse.getJSONArray("requestedData");
                        List<NotificationData> notifications = new ArrayList<>();
                        for(int i = 0; i < requestedData.length(); i++) {
                            JSONObject obj = requestedData.getJSONObject(i);
                            System.out.println("obj:\n " + obj.toString());
                            int id = obj.optInt("id", 0);
                            String description = obj.optString("description", "");
                            String date = obj.optString("date", "");
                            String time = obj.optString("time", "");
                            String projectName = obj.optString("projectName", "");
                            int projectId = obj.optInt("projectId", 0);

                            NotificationData pr = new NotificationData(id, description, date, time, projectName, projectId);
                            notifications.add(pr);
                        }
                        return notifications;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ArrayList<>();

        }
    }
}
