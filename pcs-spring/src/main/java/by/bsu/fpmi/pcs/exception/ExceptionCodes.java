package by.bsu.fpmi.pcs.exception;

/**
 * Created by Alexandra on 16.11.2017.
 */
public class ExceptionCodes {

    // Auth Exceptions
    public static final int DUPLICATE_EMAIL_EXCEPTION = 1;
    public static final int NO_SUCH_EMAIL_EXCEPTION = 2;
    public static final int INVALID_PASSWORD_EXCEPTION = 3;
    public static final int NOT_AUTHENTICATED_EXCEPTION = 4;

    // Project Exceptions
    public static final int NO_SUCH_PROJECT_EXCEPTION = 100;
    public static final int NO_ADMIN_RIGHT_EXCEPTION = 101;
    public static final int MEMBER_EXISTS_EXCEPTION = 102;
    public static final int NO_SUCH_MEMBERSHIP_EXCEPTION = 103;

    // Status Exceptions
    public static final int NO_SUCH_STATUS_EXCEPTION = 200;

    // Issue exceptions
    public static final int NO_SUCH_ISSUE_EXCEPTION = 300;

    // User exceptions
    public static final int NO_SUCH_USER_EXCEPTION = 400;

    // Role exceptions
    public static final int NO_SUCH_ROLE_EXCEPTION = 500;

    //Calendar exceptions
    public static final int NO_SUCH_EVENT_EXCEPTION = 600;
    public static final int ILLEGAL_DATE_FORMAT_EXCEPTION = 601;

    public static final int INTERNAL_SERVER_EXCEPTION = 5000;

}
