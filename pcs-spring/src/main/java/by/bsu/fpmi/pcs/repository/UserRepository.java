package by.bsu.fpmi.pcs.repository;

import by.bsu.fpmi.pcs.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Alexandra on 17.09.2017.
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByEmail(String email);
}
