import { Component, OnInit, Input } from '@angular/core';
import { IssueData } from "../../../../entities/issue-data";


@Component({
  selector: 'issue-block',
  templateUrl: './issue-block.component.html',
  styleUrls: ['./issue-block.component.scss']
})
export class IssueBlockComponent implements OnInit {
  @Input() color: string = 'red';
  @Input() issue: IssueData = new IssueData();

  constructor() {}

  ngOnInit(): void {
  }
}
