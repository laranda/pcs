/**
 * Created by Alexandra on 26.11.2017.
 */
import { UserData } from './user-data';

export class TokenData {
  userData: UserData;
  accessToken: string;
}
