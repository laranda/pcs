import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { IssueListComponent } from './issue-list.component';
import { IssueBlockComponent } from './issue-block/issue-block.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    IssueListComponent, IssueBlockComponent
  ],
  exports: [
    IssueListComponent
  ]
})
export class IssueListModule { }
