import{ Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'project-picture',
  templateUrl: './project-picture.component.html',
  styleUrls: ['./project-picture.component.scss']
})
export class ProjectPictureComponent implements OnInit {
  @Input() name: string;

  constructor() {}

  ngOnInit(): void {
  }
}
