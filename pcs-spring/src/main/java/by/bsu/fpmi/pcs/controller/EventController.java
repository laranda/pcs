package by.bsu.fpmi.pcs.controller;

import by.bsu.fpmi.pcs.dto.*;
import by.bsu.fpmi.pcs.entity.User;
import by.bsu.fpmi.pcs.service.AuthService;
import by.bsu.fpmi.pcs.service.EventService;
import by.bsu.fpmi.pcs.service.IssueService;
import by.bsu.fpmi.pcs.service.ServiceException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Alexandra on 18.11.2017.
 */
@RestController
public class EventController {

    private static Logger logger = LogManager.getLogger(EventController.class);

    @Autowired
    AuthService authService;

    @Autowired
    EventService eventService;

    @PostConstruct
    public void init() {}

    @GetMapping("/api/projects/{id}/events")
    public ResponseData getEvents(@PathVariable int id, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("GET /api/projects/{id}/events\tGetting events with token: %s", token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("GET /api/projects/{id}/events\tUser with id %d requests getting events", user.getId()));
            return new ResponseData(eventService.getEvents(user, id));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @GetMapping("/api/notifications")
    public ResponseData getNotifications(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("GET /api/notifications\tGetting notifications with token: %s", token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("GET /api/notifications\tUser with id %d requests getting notifications", user.getId()));
            return new ResponseData(eventService.getNotifications(user));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/projects/{id}/events")
    public ResponseData saveEvent(@PathVariable int id, @RequestBody EventData eventData, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("POST /api/projects/%d/events\tSave event with token: %s", id, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("POST /api/projects/%d/events\tUser with id %d requests to save event", id, user.getId()));
            return new ResponseData(eventService.saveEvent(user, eventData, id));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @DeleteMapping("/api/projects/{id}/events/{eventId}")
    public ResponseData deleteIssue(@PathVariable int id, @PathVariable int eventId, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("DELETE /api/projects/%d/events/%d\tDelete event with token: %s", id, eventId, token));
        try {
            User user = authService.auth(token);
            logger.info(String.format("DELETE /api/projects/%d/events/%d\tUser with id %d requests to delete event", id, eventId, user.getId()));
            eventService.deleteEvent(user, eventId, id);
            return new ResponseData();
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }
}
