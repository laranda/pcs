import { Component } from '@angular/core';

import { ShortProjectData } from '../entities/short-project-data';

@Component({
  selector: 'demo',
  templateUrl: './demo.component.html',
  styleUrls: []
})
export class DemoComponent {
  projects: ShortProjectData[] = [
    {
      id: 1,
      name: 'First project',
      description: 'Project description.222222222 333333333 444444444 555555555 666666666 777777777 888888888 999999999 000000000 111111111 222222222 333333333 444444444 555555555 666666666 777777777 888888888 999999999 000000000',
      monthDuration: 0,
      daysDuration: 2,
      roleName: 'PM',
      isAdmin: true
    },
    {
      id: 2,
      name: 'Second project with two-line name',
      description: 'Project description.222222222 333333333 444444444 88888888 999999999 000000000',
      monthDuration: 5,
      daysDuration: 0,
      roleName: 'Developer',
      isAdmin: false
    }
  ];
}
