/**
 * Created by Alexandra on 07.12.2017.
 */

export class IssueType {
  id: number;
  name: string;
}

export const types: IssueType[] = [
  {id: 0, name: 'Goal'},
  {id: 1, name: 'Expedite'},
  {id: 2, name: 'Task'},
  {id: 3, name: 'Bug'},
];
