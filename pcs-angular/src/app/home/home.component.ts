/**
 * Created by Alexandra on 26.11.2017.
 */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShortProjectData} from '../entities/short-project-data';
import { ProjectService } from '../services/project.service';


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  projects: ShortProjectData[] = [];
  projectsSub: any = null;

  constructor(private projectService: ProjectService) {}

  ngOnInit(): void {
    this.projectsSub = this.projectService.projectsData.subscribe(data => this.projects = data);
    this.projectService.getProjects();
  }

  ngOnDestroy(): void {
    if (this.projectsSub !== null) {
      this.projectsSub.unsubscribe();
    }
  }
}
