/**
 * Created by Alexandra on 07.12.2017.
 */

export class MembershipData {
  id: number;
  userId: number;
  userName: string;
  userEmail: string;
  projectId: number;
  roleId: number;
  roleName: string;
  admin: boolean;
}
