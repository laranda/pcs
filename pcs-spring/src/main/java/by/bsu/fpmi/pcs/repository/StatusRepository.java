package by.bsu.fpmi.pcs.repository;

import by.bsu.fpmi.pcs.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alexandra on 17.09.2017.
 */
public interface StatusRepository extends JpaRepository<Status, Integer> {
    List<Status> findByProjectId(int projectId);
    Status findByIdAndProjectId(int id, int projectId);
}
