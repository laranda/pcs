package by.bsu.pcs_android.dto;

public class ShortProjectData {

    private int id;
    private String name;
    private String description;
    private String roleName;
    private boolean isAdmin;
    private int monthDuration;
    private int daysDuration;

    public ShortProjectData(int id, String name, String description, String roleName, boolean isAdmin, int monthDuration, int daysDuration) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.roleName = roleName;
        this.isAdmin = isAdmin;
        this.monthDuration = monthDuration;
        this.daysDuration = daysDuration;
    }

    public ShortProjectData() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getMonthDuration() {
        return monthDuration;
    }

    public void setMonthDuration(int monthDuration) {
        this.monthDuration = monthDuration;
    }

    public int getDaysDuration() {
        return daysDuration;
    }

    public void setDaysDuration(int daysDuration) {
        this.daysDuration = daysDuration;
    }

}
