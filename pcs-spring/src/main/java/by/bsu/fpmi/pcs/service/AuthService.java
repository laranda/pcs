package by.bsu.fpmi.pcs.service;

import by.bsu.fpmi.pcs.dto.LoginData;
import by.bsu.fpmi.pcs.entity.Token;
import by.bsu.fpmi.pcs.entity.User;
import by.bsu.fpmi.pcs.exception.ExceptionCodes;
import by.bsu.fpmi.pcs.repository.TokenRepository;
import by.bsu.fpmi.pcs.repository.UserRepository;
import by.bsu.fpmi.pcs.utils.PasswordEncoder;
import by.bsu.fpmi.pcs.utils.TokenGenerator;
import by.bsu.fpmi.pcs.utils.UtilsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Alexandra on 12.11.2017.
 */
@Service
public class AuthService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TokenRepository tokenRepository;

    @Transactional(rollbackFor = ServiceException.class)
    public Token register(LoginData loginData) throws ServiceException {
        try {
            User user = new User();
            user.setEmail(loginData.getEmail());
            user.setName(loginData.getEmail());
            user.setPassword(PasswordEncoder.encode(loginData.getPassword()));
            userRepository.save(user);
            Token token = new Token();
            token.setAccessToken(TokenGenerator.generate());
            token.setUser(user);
            tokenRepository.save(token);
            return token;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.DUPLICATE_EMAIL_EXCEPTION,
                    String.format("Failed to register new user with email: %s", loginData.getEmail()), e);
        } catch (UtilsException e) {
            throw new ServiceException(e.getCode(), e.getMessage(), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public Token login(LoginData loginData) throws ServiceException {
        try {
            User user = userRepository.findByEmail(loginData.getEmail());
            if (user == null) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_EMAIL_EXCEPTION,
                        String.format("No user with email: %s", loginData.getEmail()));
            }
            String encodedPassword = PasswordEncoder.encode(loginData.getPassword());
            if (!user.getPassword().equals(encodedPassword)) {
                throw new ServiceException(ExceptionCodes.INVALID_PASSWORD_EXCEPTION,
                        String.format("Invalid password: %s\t Must be: %s", encodedPassword, user.getPassword()));
            }
            Token token = new Token();
            token.setAccessToken(TokenGenerator.generate());
            token.setUser(user);
            tokenRepository.save(token);
            return token;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to find user with email: %s", loginData.getEmail()), e);
        } catch (UtilsException e) {
            throw new ServiceException(e.getCode(), e.getMessage(), e);
        }
    }

    public User auth(String accessToken) throws ServiceException {
        try {
            Token token = tokenRepository.findOne(accessToken);
            if (token == null) {
                throw new ServiceException(ExceptionCodes.NOT_AUTHENTICATED_EXCEPTION,
                        String.format("No such token: %s", accessToken));
            }
            return token.getUser();
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to find token: %s", accessToken), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public void logout(String accessToken) throws ServiceException {
        try {
            Token token = tokenRepository.findOne(accessToken);
            if (token == null) {
                throw new ServiceException(ExceptionCodes.NOT_AUTHENTICATED_EXCEPTION,
                        String.format("No such token: %s", accessToken));
            }
            tokenRepository.delete(token);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to find token: %s", accessToken), e);
        }
    }
}
