package by.bsu.fpmi.pcs.service;

import by.bsu.fpmi.pcs.dto.*;
import by.bsu.fpmi.pcs.entity.*;
import by.bsu.fpmi.pcs.exception.ExceptionCodes;
import by.bsu.fpmi.pcs.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Alexandra on 22.11.2017.
 */
@Service
public class EventService {

    @Autowired
    ProjectService projectService;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    MembershipRepository membershipRepository;


    public Map<String, List<EventData>> getEvents(User user, int projectId) throws ServiceException {
        try {
            projectService.checkUserRights(user, projectId, false);
            Map<String, List<EventData>> eventsMap = new HashMap<>();
            List<EventData> eventsData = eventRepository.findByProjectIdOrderByEventTimeAsc(projectId)
                    .stream().map(EventData::new).collect(Collectors.toList());
            for (EventData event : eventsData) {
                if (!eventsMap.containsKey(event.getDate())) {
                    eventsMap.put(event.getDate(), new ArrayList<>());
                }
                eventsMap.get(event.getDate()).add(event);
            }
            return eventsMap;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get events for user with id: %d", user.getId()), e);
        }
    }

    public List<NotificationData> getNotifications(User user) throws ServiceException {
        try {
            List<Membership> memberships = membershipRepository.findByUserId(user.getId());
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            Calendar today = Calendar.getInstance();
            String todayStr = sdf.format(today.getTime());
            List<NotificationData> allNotifications = new ArrayList<>();
            for (Membership membership : memberships) {
                List<Event> events = eventRepository.findByProjectIdOrderByEventTimeAsc(membership.getProject().getId());
                List<NotificationData> notifications = events.stream().map(NotificationData::new)
                        .filter(n -> n.getDate() != null && todayStr.equals(n.getDate())).collect(Collectors.toList());
                allNotifications.addAll(notifications);
            }
            allNotifications.sort((n1, n2) -> n1.getTime().compareTo(n2.getTime()));
            return allNotifications;
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to get notifications for user with id: %d", user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public void deleteEvent(User user, int eventId, int projectId) throws ServiceException {
        try {
            projectService.checkUserRights(user, projectId, true);
            Event event = eventRepository.findOne(eventId);
            if (event == null || event.getProject().getId() != projectId) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_EVENT_EXCEPTION,
                        String.format("No event with id: %d", eventId));
            }
            eventRepository.delete(event);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to delete event with id: %d for user with id: %d", eventId, user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public EventData saveEvent(User user, EventData eventData, int projectId) throws ServiceException {
        if (eventData.getId() > 0) {
            return modifyEvent(user, eventData, projectId);
        }
        return createEvent(user, eventData, projectId);
    }

    @Transactional(rollbackFor = ServiceException.class)
    public EventData createEvent(User user, EventData eventData, int projectId) throws ServiceException {
        try {
            Membership membership = projectService.checkUserRights(user, projectId, true);
            Event event = new Event();
            event.setProject(membership.getProject());
            event.setDescription(eventData.getDescription());
            this.setEventTime(event, eventData);
            eventRepository.save(event);
            return new EventData(event);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to create new event for user with id: %d", user.getId()), e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    public EventData modifyEvent(User user, EventData eventData, int projectId) throws ServiceException {
        try {
            projectService.checkUserRights(user, projectId, false);
            Event event = eventRepository.findOne(eventData.getId());
            if (event == null || event.getProject().getId() != projectId) {
                throw new ServiceException(ExceptionCodes.NO_SUCH_EVENT_EXCEPTION,
                        String.format("No event with id: %d for project with id: %d", eventData.getId(), projectId));
            }
            event.setDescription(eventData.getDescription());
            this.setEventTime(event, eventData);
            eventRepository.save(event);
            return new EventData(event);
        } catch (DataAccessException e) {
            throw new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION,
                    String.format("Failed to update event with id: %d for user with id: %d", eventData.getId(), user.getId()), e);
        }
    }

    private void setEventTime(Event event, EventData eventData) throws ServiceException {
        if (eventData.getDate() != null && eventData.getTime() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            String dateTime = eventData.getDate() + ' ' + eventData.getTime();
            try {
                Calendar eventTime = Calendar.getInstance();
                eventTime.setTime(sdf.parse(dateTime));
                event.setEventTime(eventTime);
            } catch (ParseException e) {
                throw new ServiceException(ExceptionCodes.ILLEGAL_DATE_FORMAT_EXCEPTION,
                        String.format("Failed to create new event with date-time: %s", dateTime));
            }
        }
    }

}
