import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardModule } from './project/board/board.module';
import { TeamModule } from './project/team/team.module';
import { IssueListModule } from './project/issue-list/issue-list.module';
import { SettingsModule } from './project/settings/settings.module';
import { CalendarModule } from './calendar/calendar.module';
import { AnalyticsModule } from './analytics/analytics.module';
import { SharedModule } from '../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../router/app-routing.module';

import { BoardComponent } from './project/board/board.component';
import { TeamComponent } from './project/team/team.component';
import { IssueListComponent } from './project/issue-list/issue-list.component';
import { SettingsComponent } from './project/settings/settings.component';
import { CalendarComponent } from './calendar/calendar.component';
import { AnalyticsComponent } from './analytics/analytics.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BoardModule,
    TeamModule,
    IssueListModule,
    SettingsModule,
    CalendarModule,
    AnalyticsModule
  ],
  declarations: [],
  exports: [
    BoardComponent,
    TeamComponent,
    IssueListComponent,
    SettingsComponent,
    CalendarComponent,
    AnalyticsComponent
  ]
})
export class ProjectModule { }
