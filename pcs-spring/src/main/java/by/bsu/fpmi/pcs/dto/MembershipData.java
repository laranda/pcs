package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Membership;

/**
 * Created by Alexandra on 05.12.2017.
 */
public class MembershipData {
    private int id;
    private int userId;
    private String userName;
    private String userEmail;
    private int projectId;
    private int roleId;
    private String roleName;
    private boolean isAdmin;

    public MembershipData() {}

    public MembershipData(Membership membership) {
        this.id = membership.getId();
        this.userId = membership.getUser().getId();
        this.userName = membership.getUser().getName();
        this.userEmail = membership.getUser().getEmail();
        this.projectId = membership.getProject().getId();
        this.roleId = membership.getRole().getId();
        this.roleName = membership.getRole().getName();
        this.isAdmin = membership.getRole().isAdmin();
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
