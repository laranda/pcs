import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../router/app-routing.module';

import { ProjectPictureComponent} from './project-picture/project-pictute.component';

import { StripeBlockComponent } from './blocks/stripe-block/stripe-block.component';
import { EmptyBlockComponent } from './blocks/empty-block/empty-block.component';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

import { ProtectedDirective } from './directives/protected.directive';

import { ProjectNavComponent } from './nav/project-nav/project-nav.component';
import { SecondaryProjectNavComponent } from './nav/secondary-project-nav/secondary-project-nav.component';

import { AlertComponent} from './alert/alert.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule
  ],
  declarations: [
    ProjectPictureComponent,
    StripeBlockComponent, EmptyBlockComponent,
    FooterComponent, HeaderComponent,
    ProtectedDirective,
    ProjectNavComponent, SecondaryProjectNavComponent,
    AlertComponent
  ],
  exports: [
    ProjectPictureComponent,
    StripeBlockComponent, EmptyBlockComponent,
    FooterComponent, HeaderComponent,
    ProtectedDirective,
    ProjectNavComponent, SecondaryProjectNavComponent,
    AlertComponent
  ]
})
export class SharedModule { }
