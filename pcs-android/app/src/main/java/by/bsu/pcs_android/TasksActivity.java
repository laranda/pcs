package by.bsu.pcs_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TasksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        Bundle extras = getIntent().getExtras();
        String project_id = "";
        if (extras != null) {
            project_id = extras.getString("project_id");
        }

        Bundle bundle = new Bundle();
        bundle.putString("project_id", project_id);

        FragmentTask fragmentTask = new FragmentTask();
        fragmentTask.setArguments(bundle);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragmentTask);
        fragmentTransaction.commit();
    }
}
