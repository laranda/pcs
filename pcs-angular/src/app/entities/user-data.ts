/**
 * Created by Alexandra on 26.11.2017.
 */

export class UserData {
  id: number;
  email: string;
  name: string;
}
