package by.bsu.fpmi.pcs.repository;

import by.bsu.fpmi.pcs.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alexandra on 17.09.2017.
 */
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findByIssueId(int issueId);
}
