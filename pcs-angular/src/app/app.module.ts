import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './router/app-routing.module';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './home/home.module';
import { ProjectModule } from './project/project.module';
import { NotificationsModule } from './notifications/notifications.module';
import { MyProfileModule } from './myprofile/myprofile.module';

import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';

import { AuthService } from './services/auth.service';
import { ProjectService } from './services/project.service';
import { IssueService } from './services/issue.service';
import { RoleService } from './services/role.service';
import { EventService } from './services/event.service';
import { ErrorService } from './services/error.service';


@NgModule({
  declarations: [
    AppComponent, DemoComponent, LoginComponent, RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HomeModule,
    FormsModule,
    HttpClientModule,
    ProjectModule,
    NotificationsModule,
    MyProfileModule
  ],
  providers: [AuthService, ProjectService, IssueService, RoleService, EventService, ErrorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
