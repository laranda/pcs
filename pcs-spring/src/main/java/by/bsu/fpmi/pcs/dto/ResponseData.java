package by.bsu.fpmi.pcs.dto;

/**
 * Created by Alexandra on 18.11.2017.
 */
public class ResponseData {

    private ExceptionData exceptionData;
    private Object requestedData;

    public ResponseData() {}

    public ResponseData(Object requestedData) {
        this.requestedData = requestedData;
    }

    public ResponseData(ExceptionData exceptionData) {
        this.exceptionData = exceptionData;
    }

    public ExceptionData getExceptionData() {
        return exceptionData;
    }

    public void setExceptionData(ExceptionData exceptionData) {
        this.exceptionData = exceptionData;
    }

    public Object getRequestedData() {
        return requestedData;
    }

    public void setRequestedData(Object requestedData) {
        this.requestedData = requestedData;
    }
}
