package by.bsu.fpmi.pcs.entity;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by Alexandra on 12.11.2017.
 */
@Entity
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`")
    private int id;

    @Column(name = "`description`")
    private String description;

    @Column(name = "`event_time`")
    private Calendar eventTime;

    @ManyToOne
    @JoinColumn(name = "`project_id`")
    private Project project;

    public Calendar getEventTime() {
        return eventTime;
    }

    public void setEventTime(Calendar eventTime) {
        this.eventTime = eventTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
