package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Project;

/**
 * Created by Alexandra on 23.11.2017.
 */
public class ProjectInfoData {

    private int id;
    private String name;
    private String description;

    public ProjectInfoData() {}

    public ProjectInfoData(Project project) {
        this.id = project.getId();
        this.name = project.getName();
        this.description = project.getDescription();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
