/**
 * Created by Alexandra on 26.11.2017.
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseData } from '../entities/response-data';
import { RoleData } from '../entities/role-data';
import { environment } from '../../environments/environment';
import {ErrorService} from "./error.service";

@Injectable()
export class RoleService {

  private rolesUrl = `${environment.api.url}/api/roles`;

  rolesData: BehaviorSubject<RoleData[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient, private errorService: ErrorService){}

  getRoles() {
    this.http.get(this.rolesUrl, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue)})
      .subscribe((data: ResponseData) => {
        console.log(data);
          if (!data.exceptionData) {
            this.rolesData.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }
}

