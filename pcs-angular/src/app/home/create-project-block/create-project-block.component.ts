import { Component, OnInit} from '@angular/core';
import { ProjectInfoData } from '../../entities/project-info-data';
import { ProjectService } from '../../services/project.service';


@Component({
  selector: 'create-project-block',
  templateUrl: './create-project-block.component.html',
  styleUrls: ['./create-project-block.component.scss']
})
export class CreateProjectBlockComponent implements OnInit {
  project: ProjectInfoData = new ProjectInfoData();
  showForm: boolean = false;

  constructor(private projectService: ProjectService) {}

  ngOnInit(): void {
  }

  openForm() {
    this.showForm = true;
  }

  cancelForm() {
    this.showForm = false;
    this.project = new ProjectInfoData();
  }

  createProject() {
    this.projectService.createProject(this.project);
    this.cancelForm();
  }
}
