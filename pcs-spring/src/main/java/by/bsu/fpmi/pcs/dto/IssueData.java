package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Issue;

/**
 * Created by Alexandra on 28.11.2017.
 */
public class IssueData {

    private int id;
    private String description;
    private int priority;
    private int statusId;
    private String statusName;
    private int projectId;
    private int userId;
    private String userName;
    private String startDate;
    private String endDate;
    private int type;

    public IssueData() {}

    public IssueData(Issue issue) {
        this.id = issue.getId();
        this.description = issue.getDescription();
        this.priority = issue.getPriority();
        this.statusId = issue.getStatus() == null ? 0 : issue.getStatus().getId();
        this.statusName = issue.getStatus() == null ? null : issue.getStatus().getName();
        this.projectId = issue.getProject().getId();
        this.userId = issue.getUser() == null ? 0 : issue.getUser().getId();
        this.userName = issue.getUser() == null ? null : issue.getUser().getName();
        this.type = issue.getType();
        // TODO date-sting conversion
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
