import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router, RoutesRecognized, Event} from '@angular/router';
import { AuthService } from './services/auth.service';
import { environment } from '../environments/environment';
import { TokenData } from "./entities/token-data";
import { ProjectService } from "./services/project.service";
import { RoleService } from "./services/role.service";
import { EventService } from "./services/event.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  firstUpdate = true;

  constructor(private router: Router,
              private authService: AuthService,
              private roleService: RoleService,
              private projectService: ProjectService,
              private eventService: EventService,
              private location: Location) {
    roleService.getRoles();
    router.events.subscribe(async(event: Event) => {
        if (!(event instanceof RoutesRecognized)) {
          return;
        }
        let recEvent: RoutesRecognized = <RoutesRecognized>event;
        if (recEvent.url === '/login' || recEvent.url === '/register' || recEvent.url === '/demo') {
          this.projectService.updateCurrentProject(0);
          return;
        }
        this.checkUser(recEvent.url);
        this.updateProjectIdFromUrl(recEvent.url);
      });
    authService.tokenData.subscribe(val => {
      if (this.firstUpdate) {
        this.firstUpdate = false;
        return;
      }
      if (!this.authService.isAuthenticated()) {
        this.router.navigate(['/login']);
      } else {
        this.projectService.getProjects();
        this.eventService.getNotifications();
      }
    });
  }

  checkUser(url: string): void {
    if (this.authService.isAuthenticated()) {
      return;
    }
    let sessionData = sessionStorage.getItem(environment.auth.sessionKey);
    if (sessionData) {
      let tokenData: TokenData = JSON.parse(sessionData);
      if (tokenData && tokenData.accessToken) {
        this.authService.setTokenData(tokenData);
        return;
      }
    }
    this.location.replaceState('/');
    this.router.navigate(['/login']);
  }

  updateProjectIdFromUrl(url: string) {
    const reg = /\/project\/(\d+)\D?.*/;
    const match = url.match(reg);
    if (match) {
      this.projectService.updateCurrentProject(+match[1]);
    } else {
      this.projectService.updateCurrentProject(0);
    }
  }
}
