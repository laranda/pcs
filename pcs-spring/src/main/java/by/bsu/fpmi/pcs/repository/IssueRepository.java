package by.bsu.fpmi.pcs.repository;

import by.bsu.fpmi.pcs.entity.Issue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alexandra on 17.09.2017.
 */
public interface IssueRepository extends JpaRepository<Issue, Integer> {
    List<Issue> findByProjectIdAndType(int projectId, int type);
    List<Issue> findByProjectIdAndStatusId(int projectId, int statusId);
    List<Issue> findByProjectIdAndStatusIdAndDescriptionIgnoreCaseContaining(int projectId, int statusId, String description);
}
