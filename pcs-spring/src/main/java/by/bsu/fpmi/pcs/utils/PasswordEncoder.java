package by.bsu.fpmi.pcs.utils;

import by.bsu.fpmi.pcs.exception.ExceptionCodes;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Alexandra on 16.11.2017.
 */
public class PasswordEncoder {

    private static String b64Key = "rVcX3WhNHNN7FqyWB01vDQ==";
    private static String b64iv = "RCnEfIprBqea+lk3BZk1CQ==";

    public static String encode(String password) throws UtilsException {
        try {
            byte[] byteKey = new BASE64Decoder().decodeBuffer(b64Key);
            byte[] byteIv = new BASE64Decoder().decodeBuffer(b64iv);
            SecretKey key = new SecretKeySpec(byteKey, 0, byteKey.length, "AES");
            Cipher aesCipherForEncryption = Cipher.getInstance("AES/CFB/PKCS5Padding");
            aesCipherForEncryption.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(byteIv));
            byte[] byteDataToEncrypt = password.getBytes();
            byte[] byteCipherText = aesCipherForEncryption.doFinal(byteDataToEncrypt);
            return new BASE64Encoder().encode(byteCipherText);
        } catch (IOException | NoSuchPaddingException | InvalidAlgorithmParameterException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            throw new UtilsException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION, "Cannot encode password.", e);
        }
    }
}
