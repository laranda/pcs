package by.bsu.fpmi.pcs.entity;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by Alexandra on 12.11.2017.
 */
@Entity
@Table(name = "issue")
public class Issue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="`id`")
    private int id;

    @Column(name = "`description`")
    private String description;

    @Column(name = "`priority`")
    private int priority;

    @ManyToOne
    @JoinColumn(name = "`status_id`")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "`project_id`")
    private Project project;

    @ManyToOne
    @JoinColumn(name = "`user_id`")
    private User user;

    @Column(name = "`start_date`")
    private Calendar startDate;

    @Column(name = "`end_date`")
    private Calendar endDate;

    @Column(name = "`type`")
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
