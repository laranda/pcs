package by.bsu.fpmi.pcs.controller;

import by.bsu.fpmi.pcs.dto.ExceptionData;
import by.bsu.fpmi.pcs.dto.LoginData;
import by.bsu.fpmi.pcs.dto.ResponseData;
import by.bsu.fpmi.pcs.dto.TokenData;
import by.bsu.fpmi.pcs.service.AuthService;
import by.bsu.fpmi.pcs.service.ServiceException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Alexandra on 18.11.2017.
 */
@RestController
public class AuthController {

    private static Logger logger = LogManager.getLogger(AuthController.class);

    @Autowired
    AuthService authService;

    @PostConstruct
    public void init() {}

    @PostMapping("/api/auth/login")
    public ResponseData login(@RequestBody LoginData loginData) {
        logger.info(String.format("POST /api/auth/login\tLogin with email: %s", loginData.getEmail()));
        try {
            return new ResponseData(new TokenData(authService.login(loginData)));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/auth/register")
    public ResponseData register(@RequestBody LoginData loginData) {
        logger.info(String.format("POST /api/auth/register\tRegister with email: %s", loginData.getEmail()));
        try {
            return new ResponseData(new TokenData(authService.register(loginData)));
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

    @PostMapping("/api/auth/logout")
    public ResponseData logout(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.info(String.format("POST /api/auth/logout\tLogout with token: %s", token));
        try {
            authService.logout(token);
            return new ResponseData();
        } catch (ServiceException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(e));
        }
    }

}
