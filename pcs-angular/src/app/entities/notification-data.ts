/**
 * Created by Alexandra on 22.12.2017.
 */

export class NotificationData {
  id: number;
  description: string;
  time: string;
  date: string;
  projectId: number;
  projectName: string;
}
