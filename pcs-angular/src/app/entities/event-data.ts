/**
 * Created by Alexandra on 21.12.2017.
 */

export class EventData {
  id: number;
  description: string;
  date: string;
  time: string;
}
