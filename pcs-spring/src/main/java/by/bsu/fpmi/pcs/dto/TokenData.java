package by.bsu.fpmi.pcs.dto;

import by.bsu.fpmi.pcs.entity.Token;

/**
 * Created by Alexandra on 18.11.2017.
 */
public class TokenData {

    private String accessToken;
    private UserData userData;

    public TokenData() {}

    public TokenData(Token token) {
        this.accessToken = token.getAccessToken();
        this.userData = new UserData(token.getUser());
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
