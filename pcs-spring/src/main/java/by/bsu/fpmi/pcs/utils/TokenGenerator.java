package by.bsu.fpmi.pcs.utils;

import sun.misc.BASE64Encoder;

import java.security.SecureRandom;

/**
 * Created by Alexandra on 16.11.2017.
 */
public class TokenGenerator {

    public static String generate() {
        SecureRandom prng = new SecureRandom();
        byte[] token = new byte[32];
        prng.nextBytes(token);
        return new BASE64Encoder().encode(token);
    }
}
