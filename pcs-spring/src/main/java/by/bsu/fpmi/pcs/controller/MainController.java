package by.bsu.fpmi.pcs.controller;

import by.bsu.fpmi.pcs.dto.ExceptionData;
import by.bsu.fpmi.pcs.dto.ResponseData;
import by.bsu.fpmi.pcs.dto.RoleData;
import by.bsu.fpmi.pcs.entity.Role;
import by.bsu.fpmi.pcs.entity.User;
import by.bsu.fpmi.pcs.exception.ExceptionCodes;
import by.bsu.fpmi.pcs.repository.RoleRepository;
import by.bsu.fpmi.pcs.service.ServiceException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Alexandra on 17.09.2017.
 */
@RestController
public class MainController {

    private static Logger logger = LogManager.getLogger(MainController.class);

    @Autowired
    RoleRepository roleRepository;

    @PostConstruct
    public void init() {}

    @GetMapping("/api/roles")
    public ResponseData getRoles() {
        logger.info(String.format("GET /api/roles"));
        try {
            List<Role> roles = roleRepository.findAll();
            List<RoleData> rolesData = roles.stream().map(RoleData::new).collect(Collectors.toList());
            return new ResponseData(rolesData);
        } catch (DataAccessException e) {
            logger.error(e);
            return new ResponseData(new ExceptionData(new ServiceException(ExceptionCodes.INTERNAL_SERVER_EXCEPTION, e)));
        }
    }
}
