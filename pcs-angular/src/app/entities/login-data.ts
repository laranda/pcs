/**
 * Created by Alexandra on 26.11.2017.
 */

export class LoginData {
  email: string;
  password: string;
}
