/**
 * Created by Alexandra on 26.11.2017.
 */
import { Component, OnInit } from '@angular/core';
import { NotificationData } from "../entities/notification-data";
import { EventService } from "../services/event.service";


@Component({
  selector: 'notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  notifications: NotificationData[] = [];

  constructor(private eventService: EventService) {}

  ngOnInit(): void {
    this.notifications = this.eventService.notificationsData.getValue();
    this.eventService.notificationsData.subscribe(ns => this.notifications = ns);
    this.eventService.getNotifications();
  }
}
