/**
 * Created by Alexandra on 26.11.2017.
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseData } from '../entities/response-data';
import { BoardData } from '../entities/board-data';
import { StatusData } from '../entities/status-data';
import { IssueCriteria } from '../entities/issue-criteria';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';
import { IssueData } from "../entities/issue-data";
import { CommentData } from "../entities/comment-data";
import {ErrorService} from "./error.service";

@Injectable()
export class IssueService {

  private boardUrl = `${environment.api.url}/api/projects`;
  private issuesUrl = `${environment.api.url}/api/issues`;
  private listUrl = `${this.issuesUrl}/list`;

  boardData: Subject<BoardData> = new Subject();
  listData: Subject<StatusData> = new Subject();
  issueData: Subject<IssueData> = new Subject();
  deleted: Subject<boolean> = new Subject();
  created: Subject<boolean> = new Subject();
  commentsData: Subject<CommentData[]> = new Subject();

  constructor(private http: HttpClient, private authService: AuthService, private errorService: ErrorService){}

  getBoardData(projectId: number) {
    this.http.get(`${this.boardUrl}/${projectId}/board`, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.boardData.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  getIssueList(criteria: IssueCriteria) {
    this.http.post(this.listUrl, criteria, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.listData.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  saveIssue(issue: IssueData) {
    this.http.post(this.issuesUrl, issue, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.issueData.next(data.requestedData);
            this.created.next(true);
            this.getBoardData(issue.projectId);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  getIssue(issueId: number) {
    this.http.get(`${this.issuesUrl}/${issueId}`, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.issueData.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  deleteIssue(issueId: number, projectId: number) {
    this.http.delete(`${this.issuesUrl}/${issueId}`, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.deleted.next(true);
            this.getBoardData(projectId);
          } else {
            this.deleted.next(false);
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.deleted.next(false);
          this.errorService.showHttpCodeError(err);
        });
  }

  getComments(issueId: number) {
    this.http.get(`${this.issuesUrl}/${issueId}/comments`, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.commentsData.next(data.requestedData);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }

  addComment(issueId: number, commentData: CommentData) {
    this.http.post(`${this.issuesUrl}/${issueId}/comments`, commentData, {headers: new HttpHeaders().set(environment.api.contType, environment.api.contTypeValue).set(environment.api.accessHeader, this.authService.getAccessToken())})
      .subscribe((data: ResponseData) => {
          if (!data.exceptionData) {
            this.getComments(issueId);
          } else {
            this.errorService.showExceptionData(data.exceptionData);
          }
        },
        err => {
          this.errorService.showHttpCodeError(err);
        });
  }
}

