package by.bsu.pcs_android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import by.bsu.pcs_android.dto.IssueData;
import by.bsu.pcs_android.dto.StatusData;
import by.bsu.pcs_android.util.AccountData;
import by.bsu.pcs_android.util.UserData;

public class FragmentTask extends Fragment {

    private LoadTasksTask mLoadTask = null;
    private LoadStatusesTask mLoadStatusesTask = null;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ImageButton mCalendarButton;
    private ImageButton mSearchButton;
    private Spinner mSpinnerStatus;

    List<StatusData> statuses = new ArrayList<>();
    List<IssueData> issues = new ArrayList<>();

    private int projectId;

    public FragmentTask() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tasks, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.search_result);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ShortTasksAdapter(issues);
        mRecyclerView.setAdapter(mAdapter);

        String project_id = getArguments().getString("project_id");
        projectId = Integer.parseInt(project_id);
        mLoadStatusesTask = new LoadStatusesTask(project_id);
        mLoadStatusesTask.execute((Void) null);
        mSpinnerStatus = (Spinner) view.findViewById(R.id.spinner_status);

        mSearchButton = (ImageButton) view.findViewById (R.id.ibSearch);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String statusName = mSpinnerStatus.getSelectedItem().toString();
                int statusId = 0;
                for(StatusData data : statuses) {
                    if(data.getName().equals(statusName)) {
                        statusId = data.getId();
                    }
                }
                searchIssues(statusId);
            }
        });

        mCalendarButton = (ImageButton) view.findViewById (R.id.ibCalendar);
        mCalendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNotifications();
            }
        });
    }

    public void showNotifications() {
        Intent intent = new Intent(this.getContext(), NotificationsActivity.class);
        startActivity(intent);
    }

    public void searchIssues(int statusId) {
        mLoadTask = new LoadTasksTask(statusId, projectId);
        mLoadTask.execute((Void) null);
    }

    public void showSelectedTask(int id) {
        IssueData current = new IssueData();
        for(IssueData isd : issues) {
            if(isd.getId() == id) {
                current = isd;
            }
        }
        Intent intent = new Intent(this.getContext(), TaskActivity.class);
        intent.putExtra("description", current.getDescription());
        intent.putExtra("userName", current.getUserName());
        intent.putExtra("id", ("" + current.getId()));
        intent.putExtra("status", current.getStatusName());
        intent.putExtra("priority", ("" + current.getPriority()));
        intent.putExtra("startDate", current.getStartDate());
        int color;
        if (current.getType() == 0) {
            color = Color.YELLOW;
        } else if(current.getType() == 1) {
            color = Color.RED;
        }else if(current.getStatusId() == statuses.get(statuses.size()-1).getId()) {
            color = Color.GREEN;
        }else {
            color = Color.BLUE;
        }
        System.out.println("color = " + color);
        intent.putExtra("color", "" + color);
        startActivity(intent);
    }

    public class ShortTasksAdapter extends RecyclerView.Adapter<ShortTasksAdapter.ViewHolder> {
        public List<IssueData> mDataset;

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView tvDescription, tvUserName;
            public LinearLayout lvColoredLine;
            public int id;

            public ViewHolder(View v) {
                super(v);
                tvDescription = (TextView) v.findViewById(R.id.descriptionTB);
                tvUserName = (TextView) v.findViewById(R.id.userNameTB);
                lvColoredLine = (LinearLayout) v.findViewById(R.id.coloredLine);
                tvDescription.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showSelectedTask(id);
                    }
                });
            }
        }

        @Override
        public ShortTasksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_list_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            IssueData f = mDataset.get(position);
            holder.tvDescription.setText(f.getDescription());
            if(!f.getUserName().equals("null")) {
                holder.tvUserName.setText(f.getUserName());
            } else {
                holder.tvUserName.setText("Unpicked task.");
            }

            int type = f.getType();

            int color;
            if (type == 0) {
                color = Color.YELLOW;
            } else if(type == 1) {
                color = Color.RED;
            }else if(f.getStatusId() == statuses.get(statuses.size()-1).getId()) {
                color = Color.GREEN;
            }else {
                color = Color.BLUE;
            }

            holder.lvColoredLine.setBackgroundColor(color);
            holder.id = f.getId();
        }

        @Override
        public int getItemCount() {
            if(mDataset == null) {
                mDataset = new ArrayList<>();
            }
            return mDataset.size();
        }

        public ShortTasksAdapter(List<IssueData> myDataset) {
            mDataset = myDataset;
        }


    }

    public void showTasks(List<IssueData> issueList) {
        issues.clear();
        if(issueList.size() > 0) {
            issues.addAll(issueList);
            mAdapter.notifyDataSetChanged();
            return;
        }
        Toast.makeText(getContext(), "No tasks found.", Toast.LENGTH_LONG).show();

    }

    public void showStatuses(List<StatusData> statusesList){
        ArrayList<String> statusesNames = new ArrayList<>();
        for(StatusData data : statusesList) {
            statusesNames.add(data.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, statusesNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerStatus.setAdapter(adapter);
        statuses = statusesList;
    }

    public class LoadStatusesTask extends AsyncTask<Void, Void, List<StatusData>> {

        private String id;

        LoadStatusesTask(String id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<StatusData> statuses) {
            super.onPostExecute(statuses);
            showStatuses(statuses);
        }

        @Override
        protected List<StatusData> doInBackground(Void... voids) {
            String requestURL = AccountData.ipAddress + "/api/projects/" + id;
            URL url;
            try {
                url = new URL(requestURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Authorization", AccountData.accessToken);

                int responseCode = conn.getResponseCode();
                System.out.println("CODE " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    StringBuilder response = new StringBuilder();
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                    JSONObject jsonResponse = new JSONObject(response.toString());
                    System.out.println(response.toString());

                    if (jsonResponse.isNull("exceptionData")) {
                        JSONObject requestedData = jsonResponse.getJSONObject("requestedData");
                        JSONArray statuses = requestedData.getJSONArray("statuses");
                        List<StatusData> statusList = new ArrayList<>();

                        for(int i = 0; i < statuses.length(); i++) {
                            JSONObject obj = statuses.getJSONObject(i);
                            System.out.println("obj:\n " + obj.toString());
                            int id = obj.optInt("id", 0);
                            String name = obj.optString("name", "");
                            int order = obj.optInt("order", 0);
                            StatusData sd = new StatusData(id, name, order);
                            statusList.add(sd);
                        }
                        return statusList;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ArrayList<>();
        }
    }



    public class LoadTasksTask extends AsyncTask<Void, Void, List<IssueData>> {
        private int statusId;
        private int projectId;

        private String loginReq;

        private void prepareLoginReq() {
            JSONObject reqJson = new JSONObject();
            try {
                reqJson.put("projectId", projectId);
                reqJson.put("statusId", statusId);
                reqJson.put("description", "");
            } catch (JSONException e) {}
            loginReq = reqJson.toString();
        }

        public LoadTasksTask(int statusId, int projectId) {
            this.statusId = statusId;
            this.projectId = projectId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<IssueData> issues) {
            super.onPostExecute(issues);
            showTasks(issues);
        }

        @Override
        protected List<IssueData> doInBackground(Void... voids) {
            String requestURL = AccountData.ipAddress + "/api/issues/list";
            URL url;
            try {
                url = new URL(requestURL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", AccountData.accessToken);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                prepareLoginReq();
                writer.write(loginReq);

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                System.out.println("CODE " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    StringBuilder response = new StringBuilder();
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                    JSONObject jsonResponse = new JSONObject(response.toString());
                    System.out.println(response.toString());
                    if (jsonResponse.isNull("exceptionData")) {
                        JSONObject requestedData = jsonResponse.getJSONObject("requestedData");
                        JSONArray issues = requestedData.getJSONArray("issues");
                        List<IssueData> issueList = new ArrayList<>();

                        for(int i = 0; i < issues.length(); i++) {
                            JSONObject obj = issues.getJSONObject(i);
                            System.out.println("obj:\n " + obj.toString());
                            int id = obj.optInt("id", 0);
                            String description = obj.optString("description", "");
                            int priority = obj.optInt("priority", 0);
                            int statusId = obj.optInt("statusId", 0);
                            String statusName = obj.optString("statusName", "");
                            int projectId = obj.optInt("projectId", 0);
                            int userId = obj.optInt("userId", 0);
                            String userName = obj.optString("userName", "");
                            String startDate = obj.optString("startDate", "");
                            String endDate = obj.optString("endDate", "");
                            int type = obj.optInt("type", 0);
                            IssueData isd = new IssueData(id, description, priority, statusId, statusName, projectId, userId, userName, startDate, endDate, type);
                            issueList.add(isd);
                        }
                        return issueList;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ArrayList<>();
        }
    }

}
