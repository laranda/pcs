import{ Component, OnInit, Input } from '@angular/core';
import {ErrorService} from "../../services/error.service";


@Component({
  selector: 'alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  errorMessage: string = null;

  constructor(private errorService: ErrorService) {
    this.errorService.errorMessage.subscribe(m => this.errorMessage = m);
  }

  ngOnInit(): void {
  }

  onCancel() {
    this.errorService.hideErrorMessage();
  }
}
