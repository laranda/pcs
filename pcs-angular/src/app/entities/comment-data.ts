/**
 * Created by Alexandra on 07.12.2017.
 */

export class CommentData {
  id: number;
  content: string;
  issueId: number;
  creationTime: string;
  userId: number;
  userName: string;
}
