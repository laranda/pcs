package by.bsu.fpmi.pcs.entity;

/**
 * Created by Alexandra on 28.11.2017.
 */
public class IssueType {
    public static final int GOAL = 0;
    public static final int EXPEDITE = 1;
    public static final int TASK = 2;
    public static final int BUG = 3;
}
